/**
 * VERSION: 2.1
 * DATE: 
 * JS
 * AUTHOR: Ian Duff
 * COPYRIGHT: Essemble Ltd
 * All code © 2015 Essemble Ltd. all rights reserved
 * This code is the property of Essemble Ltd and cannot be copied, reused or modified without prior permission
 **/
function extend(t, e) {
    var i = e.prototype;
    e.prototype = Object.create(t.prototype);
    for (var n in i) e.prototype[n] = i[n];
    e.prototype.constructor = e, Object.defineProperty(e.prototype, "constructor", {
        enumerable: !1,
        value: e
    })
}

function get(t) {
    return document.getElementById(t)
}

function getJQ(t) {
    return $("#" + t)
}

function loadXML(t, e) {
    $.ajax({
        type: "GET",
        url: t,
        dataType: "xml",
        error: xmlError,
        success: e       //dc -- looks like the whole object is returned here at the very beginning //dc 
    })
}

function xmlError(t, e) {
    alert("xmlError " + e)
}

function checkColour(t) {
    var e = null;
    switch (t) {
        case "":
            break;
        default:
            if (-1 != t.indexOf("rgb")) e = t;
            else {
                var e = t.replace("0x", "");
                "#" != e.charAt(0) && (e = "#" + e)
            }
    }
    return e
}

function xmlAttr(t, e) {
    var i = !1;
    return i = t.attr(e) ? !0 : !1
}

function xmlAttrNum(t, e) {
    var i = !1;
    if (xmlAttr(t, e)) {
        var n = t.attr(e);
        i = isNaN(parseInt(n)) ? !1 : !0
    }
    return i
}

function xmlAttrStr(t, e) {
    var i = !1;
    if (xmlAttr(t, e)) {
        var n = t.attr(e);
        "" != n ? i = !0 : !1
    }
    return i
}

function create(t) { // creates html  for the quiz function //dc

    var e = document.createElement(t.type);
	
	/*
	console.log("t create dump is" );
	console.log(t);
	
	console.log("t.type is:");
	console.log(t.type);

	console.log("e variable is");
	console.log(e);
	*/
  
	
	if(t.className == 'nameplate') {
	
	    t.className == '__nameplate';
		t.innerHTML = t.text; //'<label>' + t.text  + '</label>'; //+ t.customHTML ;
		
	}
	
	
    return t.id && (e.id = t.id), 
	       t.className && (e.className = t.className), 
		   t.imgSrc && (e.src = t.imgSrc), 
		   t.href && (e.href = t.href), 
		   t.inputType && (e.type = t.inputType), 
		   t.inputValue && (e.value = t.inputValue), 
		   t.title && (e.title = t.title, e.alt = t.title), 
		   t.onMouseOver && (e.onmouseover = t.onMouseOver), 
		   t.onMouseOut && (e.onmouseout = t.onMouseOut), 
		   t.onMouseDown && (e.onmousedown = t.onMouseDown), 
		   t.onFocus && (e.onfocus = t.onFocus), 
		   t.onBlur && (e.onblur = t.onBlur), 
		   t.onClick && (e.onclick = t.onClick), 
		   t.tabIndex && (e.tabIndex = t.tabIndex), 
		   t.accessKey && (e.accessKey = t.accessKey), 
		   t.innerHTML && (e.innerHTML = t.innerHTML), e
}


function getAllSiblings(elem, filter) { //dc
    var sibs = [];
    elem = elem.parentNode.firstChild;
    do {
        if (!filter || filter(elem)) sibs.push(elem);
		
		//console.log("sib is");
		//console.log(elem);
		
    } while (elem = elem.nextSibling)
	
	
    return sibs;
}


function hexToR(t) {
    return parseInt(cutHex(t).substring(0, 2), 16)
}

function hexToG(t) {
    return parseInt(cutHex(t).substring(2, 4), 16)
}

function hexToB(t) {
    return parseInt(cutHex(t).substring(4, 6), 16)
}

function cutHex(t) {
    var e = t;
    return "#" == t.charAt(0) && (e = t.substring(1, 7)), -1 != t.indexOf("0x") && (e = t.substring(2, 8)), e
}

function hexToRgb(t) {
    var e = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    t = t.replace(e, function(t, e, i, n) {
        return e + e + i + i + n + n
    });
    var i = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t);
    return i ? {
        r: parseInt(i[1], 16),
        g: parseInt(i[2], 16),
        b: parseInt(i[3], 16)
    } : null
}

function supportsRGBA() {
    var t = !1,
        e = document.createElement("div");
    return e.style.cssText = "background-color:rgba(150,255,150,.5)", ~("" + e.style.backgroundColor).indexOf("rgba") && (t = !0), t
}

function replaceXMLStr(t, e, i) {
    e = e.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
    var n = new RegExp(e, "g"),
        s = $(t).text().replace(n, i);
    return $(t).text(s), t
}

function JQEase(t) {
    t || (t = "easeOutQuad");
    var e = t;
    if (t.indexOf(".") > -1) {
        var i = t.split("."),
            n = i[0],
            s = i[1];
        "Regular" == n && (n = "Quad"), e = s + n
    }
    return e
}

function getUnit(t) {
    return -1 == t.indexOf("%") && -1 == t.indexOf("px") && -1 == t.indexOf("em") && (t += "px"), t
}

function Screen(t) { //now passing in a session object to track returning users //dc  // this is the main game object
    this._container, 
	this._id = t.session.user_id,
	this._username = t.session.user_name,
	this._current_slide = t.session.current_slide,
	this._token = t.session.token;
	
	//console.log("SESSION TOKEN IS" +  this._token);
	
	this._xmlPath = t.xmlPath, 
	this._session = t.session,   //dc
	
	//console.log("session is NOW " );
	//console.log(this._session);
	
	this._arEvents = [], 
	this._screenElement = null, 
	this._elementLoader = null, 
	t.screenElement && (this._screenElement = t.screenElement), 
	xmlAttrStr($(t.screenXML), "xml") && (this._xmlPath = $(t.screenXML).attr("xml"), -1 == this._xmlPath.indexOf(".xml") && (this._xmlPath += ".xml")), 
	
	this.loadXMLCompleteHandler = function(t) { // once xml loaded, set up for events 
       
	    this._xml = $(t), this.createElements($(t)),  // 
		
		this.doClickEventById("screenloading", null); 
		//console.log(t);  // this is the whole game 
		//console.log("screen fired"); //dc
		
    }
}


function Screen__HOLD(t) {  //ORIG
    this._container, this._id = t.id, this._xmlPath = t.xmlPath, this._arEvents = [], this._screenElement = null, this._elementLoader = null, t.screenElement && (this._screenElement = t.screenElement), xmlAttrStr($(t.screenXML), "xml") && (this._xmlPath = $(t.screenXML).attr("xml"), -1 == this._xmlPath.indexOf(".xml") && (this._xmlPath += ".xml")), this.loadXMLCompleteHandler = function(t) {
        this._xml = $(t), this.createElements($(t)), this.doClickEventById("screenloading", null)
    }
}



function PxLoaderImage(t, e, i) {
    var n = this,
        s = null;
    this.img = new Image, this.tags = e, this.priority = i;
    var r = function() {
            "complete" === n.img.readyState && (l(), s.onLoad(n))
        },
        a = function() {
            l(), s.onLoad(n)
        },
        o = function() {
            l(), s.onError(n)
        },
        l = function() {
            n.unbind("load", a), n.unbind("readystatechange", r), n.unbind("error", o)
        };
    this.start = function(e) {
		
		//console.log("start 1 function called");
		
        s = e, n.bind("load", a), n.bind("readystatechange", r), n.bind("error", o), n.img.src = t
    }, this.checkStatus = function() {
        n.img.complete && (l(), s.onLoad(n))
    }, this.onTimeout = function() {
        l(), n.img.complete ? s.onLoad(n) : s.onTimeout(n)
    }, this.getName = function() {
        return t
    }, this.bind = function(t, e) {
        n.img.addEventListener ? n.img.addEventListener(t, e, !1) : n.img.attachEvent && n.img.attachEvent("on" + t, e)
    }, this.unbind = function(t, e) {
        n.img.removeEventListener ? n.img.removeEventListener(t, e, !1) : n.img.detachEvent && n.img.detachEvent("on" + t, e)
    }
}

function Box(t) {
	
	// important loads the respective xml boxes //dc 
	
	 //console.log("box() called");
	
	 //console.log("box t element is");
	 //console.log(t.element);
	
	// return false;
	
    if (this._element = t.element, this._screen = t.element._screen, this._container = t.container, this._xml = t.xml, this._width = 0, this._height = 0, this._scroll = !1, this._element._width && (this._width = this._element._width), this._element._height && (this._height = this._element._height), xmlAttr(this._xml, "scroll") && (this._scroll = Boolean(-1 != this._xml.attr("scroll").toLowerCase().indexOf("true"))), this._xml.children().length > 0)
        
		
		//console.log("this xml is");
		//console.log(this._xml);
		
		for (var e = 0; e < this._xml.children().length; e++) {
		
		   //console.log("xml children e is: ");
		   //console.log(this._xml.children()[e]);
		  
		   // console.log("The current xml child is: ");
		 
		   // console.log(this._xml.children()[e]);
		 
            var i = this._xml.children()[e],
			
			
                n = t.element._elementLoader,
                s = new Element({
                    screen: this._screen,
                    xml: i
                });
				
				
            xmlAttr($(i), "target") || (s._target = this._container), n.addElement(s)
        }
}

function Button(t) {
    this._screen = t.element._screen, this._container = t.container, this._xml = t.xml, this._xpad = 10, this._ypad = 6;
    
	if(this._xml.attr("class") != 'resume') { //dc
	
		var e = create({
			type: "input",
			inputType: "button",
			id: "btn",
			className:this._xml.attr("class"),  //dc // enables adding a class to a button
			//innerHTML:this._xml.attr("html_insert"),
			inputValue: $(this._xml).text().replace(/<[^>]*>/g, "")
			
		});
	
	    this._container.appendChild(e), $(e).css("width", "auto"), $(e).css("height", "auto"), xmlAttrNum(this._xml, "width") && $(e).css("width", this._xml.attr("width")), xmlAttrNum(this._xml, "height") && $(e).css("height", this._xml.attr("height") &&  this._xml.attr("class"))
	
	}
	
	
	if(this._xml.attr("class") == 'resume' && this._screen._current_slide > 2 ) {  //dc
	
	  console.log("current slide is greater than 2. It's : ");
	  console.log(this._screen._current_slide);
	
	
		var e = create({
			type: "input",
			inputType: "button",
			id: "btn",
			className:this._xml.attr("class"),  //dc // enables adding a class to a button
			//innerHTML:this._xml.attr("html_insert"),
			inputValue: $(this._xml).text().replace(/<[^>]*>/g, "")
			
		});
	
	    this._container.appendChild(e), $(e).css("width", "auto"), $(e).css("height", "auto"), xmlAttrNum(this._xml, "width") && $(e).css("width", this._xml.attr("width")), xmlAttrNum(this._xml, "height") && $(e).css("height", this._xml.attr("height") &&  this._xml.attr("class"))
	
	}
	
	
   
}

function Section(t) { // new html element creation function //dc
    this._screen = t.element._screen, this._container = t.container, this._xml = t.xml, this._xpad = 10, this._ypad = 6;
    var e = create({
        type: "section",
        inputType: "",
        id: "",
		text:this._screen._username,
		className:this._xml.attr("class"),
		//customHTML:'<img src="info_slides/images/part2/certificate.jpg" class="full-width img-responsive"/>',//this._xml.attr("html_insert"),
        inputValue: $(this._xml).text().replace(/<[^>]*>/g, "")
		
    });
	
    this._container.appendChild(e), $(e).css("width", "auto"), $(e).css("height", "auto"), xmlAttrNum(this._xml, "width") && $(e).css("width", this._xml.attr("width")), xmlAttrNum(this._xml, "height") && $(e).css("height", this._xml.attr("height") &&  this._xml.attr("class"))
}



function Label(t) { // new html element creation function //dc
  
    this._screen = t.element._screen, this._container = t.container, this._xml = t.xml, this._xpad = 10, this._ypad = 6;
    var e = create({
        type: "label",
        inputType: "",
        id: "",
		text:this._screen._username,
		className:this._xml.attr("class"),
		//customHTML:'<img src="info_slides/images/part2/certificate.jpg" class="full-width img-responsive"/>',//this._xml.attr("html_insert"),
        inputValue: $(this._xml).text().replace(/<[^>]*>/g, "")
		
    });
	
	//console.log("this input value is");
	//console.log($(this._xml).text().replace(/<[^>]*>/g, ""));
	
	console.log("this container is");
	console.log(this._container.appendChild(e));
	
	console.log("e is : ");
	console.log(e);
	
    this._container.appendChild(e), $(e).css("width", "auto"), $(e).css("height", "auto"), xmlAttrNum(this._xml, "width") && $(e).css("width", this._xml.attr("width")), xmlAttrNum(this._xml, "height") && $(e).css("height", this._xml.attr("height") &&  this._xml.attr("class"))
}




function Element(t) {
    this._screen = t.screen, 
	this._xml = $(t.xml), 
	this._type = t.xml.tagName || "sprite", 
	this._elementLoader = t.loader || this._screen._elementLoader, 
	this._id = this._xml.attr("id") || t.id || this.createInstanceName(), 
	this._relativePos = !1, this._x = 0, 
	this._y = 0, this._width = null, 
	this._height = null, this._target = null, this._container = null, 
	this._scroll = !1, this._anim = "none", 
	this._animease = "Regular.easeOut", 
	this._animtime = .5, 
	this._animdelay = 0, 
	this._animOnComplete, 
	this._eventq = !0, 
	this._event = null, 
	this._arRollovers = [], 
	this._arRollouts = [], 
	this._arClicks = [], 
	this._custom = null, 
	this._initialAnimComplete = !1, 
	this._elementScreen = null, 
	xmlAttrStr(this._xml, "x") && (this._x = this._xml.attr("x")), 
	xmlAttrStr(this._xml, "y") && (this._y = this._xml.attr("y")), 
	t.x && (this._x = t.x), t.y && (this._y = t.y), 
	xmlAttrStr(this._xml, "width") && (this._width = this._xml.attr("width")), 
	xmlAttrStr(this._xml, "height") && (this._height = this._xml.attr("height")), 
	t.width && (this._width = t.width), t.height && (this._height = t.height), 
	this._filter = this._xml.attr("filter") || null, xmlAttrStr(this._xml, "scroll") && (this._scroll = Boolean(-1 != this._xml.attr("scroll").toLowerCase().indexOf("true"))), 
	xmlAttrStr(this._xml, "anim") && (this._anim = this._xml.attr("anim").toLowerCase()), xmlAttrNum(this._xml, "animrotate") && (this._animrotate = this._xml.attr("animrotate")), xmlAttrStr(this._xml, "ease") && (this._animease = this._xml.attr("ease")), xmlAttrNum(this._xml, "animtime") && (this._animtime = parseFloat(this._xml.attr("animtime"))), xmlAttrNum(this._xml, "animdelay") && (this._animdelay = parseFloat(this._xml.attr("animdelay"))), xmlAttrStr(this._xml, "animcomplete") && (this._animOnComplete = this._xml.attr("animcomplete")), xmlAttrStr(this._xml, "event") && (this._event = this._xml.attr("event")), xmlAttrStr(this._xml, "eventq") && (this._eventq = Boolean("false" != this._xml.attr("eventq").toLowerCase()))
  
  if(this._id == 'certificate_award') {
    console.log("new element ID is");
	console.log(this._id);
  }

}

function ElementLoader(t) {
	
	// this is the very first function to fire //dc
	
	
	
    var e = this;
    this._screen = t.screen, this._arElements = t.elements || [], this._onLoadComplete = t.onLoadComplete || null, this._onAnimsComplete = t.onAnimsComplete || null, this._onCompleteScope = t.onCompleteScope || this._screen, this._onCompleteParams = t.onCompleteParams || null, this._preloader = null, this._loadQueue, this._batchFinished = !1, this._loader = new PxLoader;
    
	
	for (var i = 0; i < this._arElements.length; i++) this._arElements[i]._elementLoader = this;
    
	
	this._loader.addProgressListener(function(t) {
        var e = new Image;
        e.src = t.resource.getName();
        var i = t.resource.element;
		
		
		
        xmlAttrStr(i._xml, "className") && $(e).addClass(i._xml.attr("className")), xmlAttrStr(i._xml, "class") && $(e).addClass(i._xml.attr("class")), $(i._container).append(e)
    }), this._loader.addCompletionListener(function() {
		
		            //console.log("pre loader hidden"); //dc
					//return false;
					
                    e.hidePreloader(), 
					e._onLoadComplete && (e._onCompleteParams ? e._onCompleteScope[e._onLoadComplete](e._onCompleteParams) : e._onCompleteScope[e._onLoadComplete]());
					
					
        for (var t = 0; t < e._arElements.length; t++) e._arElements[t]._relativePos ? e.delayRelativePos(e._arElements[t]) : e.delayAnimate(e._arElements[t] /* the initial animation of the first slide  //dc */ )
    })
	 
	
}

function Text(t) {
    this._element = t.element, this._screen = this._element._screen, this._container = t.container, this._xml = t.xml, $(this._container).append(this._xml.text())
}
var supportsTouch = "ontouchstart" in window || navigator.msMaxTouchPoints,
    bRGBASupported = supportsRGBA(),
    ua = navigator.userAgent.toLowerCase(),
    isiPad = null != ua.match(/ipad/i),
    isiPhone = null != ua.match(/iphone/i),
    isChrome = null != ua.match(/chrome/i);
null !== ua.match(/android 2\.[12]/) && (HTMLMediaElement.prototype.canPlayType = function(t) {
        return null !== t.match(/video\/(mp4|m4v)/gi) ? "maybe" : ""
    }), 
	
	
	
	
	
	Screen.prototype = {
       
	   
	   load: function(t, e) {
			
			//console.log("initial load called"); // the initial load function //dc
			
			
            this._elementLoader && (this._elementLoader._arElements = null, this._elementLoader = null), 
			this._container = t, 
			e || this.unload(this._container), 
			this._arEvents = [], 
			this._xmlPath ? loadXML(this._xmlPath, this.loadXMLCompleteHandler.bind(this)) : alert("xml file has not been defined")
       
	      
	   
	    },
        screenIsElement: function() {
            var t = !1;
            return this._screenElement && (t = !0), t
        },
		
		
        createElements: function(t) { //dc
            var e;
            this._elementLoader = new ElementLoader(this.screenIsElement() ? {
                screen: this,
                elements: [],
                onAnimsComplete: "screenAnimsComplete",
                onCompleteParams: [],
                onCompleteScope: this
            } : {
                screen: this,
                elements: [],
                onLoadComplete: "screenLoadComplete",
                onAnimsComplete: "screenAnimsComplete",
                onCompleteParams: [],
                onCompleteScope: this
            });
            var i = this;
            
			$(t).find("data").children().each(function() {
                switch (this.tagName.toLowerCase()) {
                    case "events":
                        i.createEvents(this);
                        break;
                    default:
                        e = new Element({
                            screen: i,
                            xml: this
                        }), i.getElementLoaderArray().push(e)
                }
            }), this._elementLoader.load()
        },
		
		
        createEvents: function(t) {
            var e = this;
            $(t).find("event").each(function() {
                var t = $(this).attr("id"),
                    i = [],
                    n = [],
                    s = [];
                $(this).find("rollover").children().each(function() {
                    i.push($(this))
                }), $(this).find("rollout").children().each(function() {
                    n.push($(this))
                }), $(this).find("click").children().each(function() {
                    s.push($(this))
                });
                var r = {};
                r._id = t, r._arRollovers = i, r._arRollouts = n, r._arClicks = s, e._arEvents.push(r)
            })
        },
        screenLoadComplete: function() { console.log("screen load completed !! screenLoadComplete()"); /*dc */ },
        screenAnimsComplete: function() {
            this.doClickEventById("screenloaded", null)
        },
        container: function() {
            return this._container
        },
        unload: function(t) {
            t.innerHTML = "", this._elementLoader && this._elementLoader.stop()
        },
        hide: function() {
            $(this._container).hide()
        },
        show: function() {
            $(this._container).show()
        },
        getElementLoaderArray: function() {
            return this._elementLoader.getElements()
        },
        getElementById: function(t) {
            for (var e = null, i = this.getElementLoaderArray(), n = 0; n < i.length; n++)
                if (i[n]._id == t) {
                    e = i[n];
                    break
                }
            return e
        },
        getElementByType: function(t) {
            for (var e = null, i = this.getElementLoaderArray(), n = 0; n < i.length; n++)
                if (i[n]._type == t) {
                    e = i[n];
                    break
                }
            return e
        },
		
		
		getSession:function(t) {
			
			return t.session;
			
		},
		
        doClickEventById: function(t, e) {
            for (var i = t.split(","), n = 0; n < i.length; n++) {
                var t = i[n],
                    s = this.getScreen(t),
                    r = this.getElementId(t),
                    a = s.getEventById(r);
                a && a._arClicks.length > 0 && s.fireEvents(a._arClicks, e)
            }
        },
        getEventById: function(t) {
            for (var e = null, i = 0; i < this._arEvents.length; i++)
                if (this._arEvents[i]._id == t) {
                    e = this._arEvents[i];
                    break
                }
            return e
        },
        getScreen: function(t) {
            var e = this;
            return -1 != t.indexOf("parent.") && (e = this._screenElement._screen), -1 != t.indexOf("screen.") && (e = this._course.curScreen()), e
        },
        getElementId: function(t) {
            var e = t;
            return -1 != t.indexOf(".") && (e = t.split(".")[1]), e
        },
        fireEvents: function(t, e) {
            for (var i = 0; i < t.length; i++) {
                var n, s, r = $(t[i]),
                    a = r.text().replace(/\s+/g, "").toString(),
                    o = a.split(",");
                switch (r[0].tagName) {
                    case "event":
                        for (var l = 0; l < o.length; l++) n = o[l], this.getScreen(n).doClickEventById(this.getElementId(n), e);
                        break;
                    case "css":
                        for (var h = r.attr("name").toString(), c = 0; c < o.length; c++) n = o[c], s = "this" == n ? e : this.getScreen(n).getElementById(this.getElementId(n)), $(s._container).removeClass(), $(s._container).addClass(h);
                        break;
                    case "url":
                        var m = r.attr("address"),
                            _ = window.open(m);
                        _.focus();
                        break;
                    case "anim":
                        for (var u = 0; u < o.length; u++) {
                            n = o[u], s = "this" == n ? e : this.getScreen(n).getElementById(this.getElementId(n));
                            var d = s._anim;
                            s._anim = r.attr("type"), s._animtime = r.attr("animtime"), s._animdelay = r.attr("animdelay"), s._animease = r.attr("ease"), s._animOnComplete = r.attr("animcomplete") || null, s.animate(), s._anim = d
                        }
                        break;
                    case "enable":
                        for (var f = 0; f < o.length; f++) n = o[f], s = "this" == n ? e : this.getScreen(n).getElementById(this.getElementId(n)), s.enable();
                        break;
                    case "disable":
                        for (var p = 0; p < o.length; p++) n = o[p], s = "this" == o[p] ? e : this.getScreen(n).getElementById(this.getElementId(n)), s.disable();
                        break;
                    case "function":
                        var g = r.attr("name").toString(),
                            x = r.text(),
                            v = this;
                        "" == x && (x = null), "this" == x && (x = e);
                        var y;
                        if (-1 != g.indexOf(".")) {
                            var E = g.split(".");
                            if ("parent" == E[0].toLowerCase()) {
                                var b = this._screenElement;
                                b && (v = b.screen())
                            } else {
                                var y = this.getElementById(E[0]);
                                y && y._elementScreen && (v = y._elementScreen)
                            }
                            g = E[1]
                        }
                        v.searchForFunction(g, x)
                }
            }
        },
        searchForFunction: function(t, e) {
            for (var i = 0; i < this.getElementLoaderArray().length; i++) {
                var n = this.getElementLoaderArray()[i];
                if (n._custom && "function" == typeof n._custom[t]) {
                    e ? n._custom[t](e, this) : n._custom[t](this);
                    break
                }
            }
            "function" == typeof this[t] && (e ? this[t](e, this) : this[t](this)), "function" == typeof window[t] && (e ? window[t](e, this) : window[t](this))
        },
        id: function() {
            return this._id
        },
        xmlPath: function() {
            return this._xmlPath
        }
    },
    function(t) {
        function e(t) {
            t = t || {}, this.settings = t, null == t.statusInterval && (t.statusInterval = 5e3), null == t.loggingDelay && (t.loggingDelay = 2e4), null == t.noProgressTimeout && (t.noProgressTimeout = 1 / 0);
            var e, n = [],
                s = [],
                r = Date.now(),
                a = {
                    QUEUED: 0,
                    WAITING: 1,
                    LOADED: 2,
                    ERROR: 3,
                    TIMEOUT: 4
                },
                o = function(t) {
                    return null == t ? [] : Array.isArray(t) ? t : [t]
                };
            this.add = function(t) {
                t.tags = new i(t.tags), null == t.priority && (t.priority = 1 / 0), n.push({
                    resource: t,
                    status: a.QUEUED
                })
            }, this.addProgressListener = function(t, e) {
                s.push({
                    callback: t,
                    tags: new i(e)
                })
            }, this.addCompletionListener = function(t, e) {
                s.push({
                    tags: new i(e),
                    callback: function(e) {
                        e.completedCount === e.totalCount && t(e)
                    }
                })
            };
            var l = function(t) {
                t = o(t);
                var e = function(e) {
                    for (var i = e.resource, n = 1 / 0, s = 0; s < i.tags.length; s++)
                        for (var r = 0; r < Math.min(t.length, n) && !(i.tags.all[s] === t[r] && n > r && (n = r, 0 === n)) && 0 !== n; r++);
                    return n
                };
                return function(t, i) {
                    var n = e(t),
                        s = e(i);
                    return s > n ? -1 : n > s ? 1 : t.priority < i.priority ? -1 : t.priority > i.priority ? 1 : 0
                }
            };
            this.start = function(t) {
				
				//console.log("start 2 called"); // called right after load  //dc
				
                if (0 == n.length) try {
                    listener = s[1], listener.callback({
                        completedCount: 0,
                        totalCount: 0
                    })
                } catch (i) {}
                e = Date.now();
                var r = l(t);
                n.sort(r);
                for (var o = 0, c = n.length; c > o; o++) {
                    var m = n[o];
                    m.status = a.WAITING, m.resource.start(this)
					
					//console.log("this context here is " );
					//console.log(this);
					
                }
                setTimeout(h, 100)
            };
            var h = function() {
                for (var e = !1, i = Date.now() - r, s = i >= t.noProgressTimeout, o = i >= t.loggingDelay, l = 0, c = n.length; c > l; l++) {
                    var m = n[l];
                    m.status === a.WAITING && (m.resource.checkStatus && m.resource.checkStatus(), m.status === a.WAITING && (s ? m.resource.onTimeout() : e = !0))
                }
                o && e && _(), e && setTimeout(h, t.statusInterval)
            };
            this.isBusy = function() {
                for (var t = 0, e = n.length; e > t; t++)
                    if (n[t].status === a.QUEUED || n[t].status === a.WAITING) return !0;
                return !1
            };
            var c = function(t, e) {
                var i, o, l, h, c, _ = null;
                for (i = 0, o = n.length; o > i; i++)
                    if (n[i].resource === t) {
                        _ = n[i];
                        break
                    }
                if (null != _ && _.status === a.WAITING)
                    for (_.status = e, r = Date.now(), l = t.tags.length, i = 0, o = s.length; o > i; i++) h = s[i], c = 0 === h.tags.length ? !0 : t.tags.intersects(h.tags), c && m(_, h)
            };
            this.onLoad = function(t) {
                c(t, a.LOADED)
            }, this.onError = function(t) {
                c(t, a.ERROR)
            }, this.onTimeout = function(t) {
                c(t, a.TIMEOUT)
            };
            var m = function(t, e) {
                    var i, s, r, o, l = 0,
                        h = 0;
                    for (i = 0, s = n.length; s > i; i++) r = n[i], o = !1, o = 0 === e.tags.length ? !0 : r.resource.tags.intersects(e.tags), o && (h++, (r.status === a.LOADED || r.status === a.ERROR || r.status === a.TIMEOUT) && l++);
                    e.callback({
                        resource: t.resource,
                        loaded: t.status === a.LOADED,
                        error: t.status === a.ERROR,
                        timeout: t.status === a.TIMEOUT,
                        completedCount: l,
                        totalCount: h
                    })
                },
                _ = this.log = function(t) {
                    if (window.console) {
                        var i = Math.round((Date.now() - e) / 1e3);
                        window.console.log("PxLoader elapsed: " + i + " sec");
                        for (var s = 0, r = n.length; r > s; s++) {
                            var o = n[s];
                            if (t || o.status === a.WAITING) {
                                var l = "PxLoader: #" + s + " " + o.resource.getName();
                                switch (o.status) {
                                    case a.QUEUED:
                                        l += " (Not Started)";
                                        break;
                                    case a.WAITING:
                                        l += " (Waiting)";
                                        break;
                                    case a.LOADED:
                                        l += " (Loaded)";
                                        break;
                                    case a.ERROR:
                                        l += " (Error)";
                                        break;
                                    case a.TIMEOUT:
                                        l += " (Timeout)"
                                }
                                o.resource.tags.length > 0 && (l += " Tags: [" + o.resource.tags.all.join(",") + "]"), window.console.log(l)
                            }
                        }
                    }
                }
        }

        function i(t) {
            if (this.all = [], this.first = null, this.length = 0, this.lookup = {}, t) {
                if (Array.isArray(t)) this.all = t.slice(0);
                else if ("object" == typeof t)
                    for (var e in t) t.hasOwnProperty(e) && this.all.push(e);
                else this.all.push(t);
                this.length = this.all.length, this.length > 0 && (this.first = this.all[0]);
                for (var i = 0; i < this.length; i++) this.lookup[this.all[i]] = !0
            }
        }
        i.prototype.intersects = function(t) {
            if (0 === this.length || 0 === t.length) return !1;
            if (1 === this.length && 1 === t.length) return this.first === t.first;
            if (t.length < this.length) return t.intersects(this);
            for (var e in this.lookup)
                if (t.lookup[e]) return !0;
            return !1
        }, "function" == typeof define && define.amd && define("PxLoader", [], function() {
            return e
        }), t.PxLoader = e
    }(this), Date.now || (Date.now = function() {
        return (new Date).getTime()
    }), Array.isArray || (Array.isArray = function(t) {
        return "[object Array]" === Object.prototype.toString.call(t)
    }), PxLoader.prototype.addImage = function(t, e, i) {
        var n = new PxLoaderImage(t, e, i);
        return this.add(n), n.img
    }, "function" == typeof define && define.amd && define("PxLoaderImage", [], function() {
        return PxLoaderImage
    }), Element.prototype = {
        elementLoader: function() {
            return this._elementLoader
        },
        createInstanceName: function() {
            return "instance" + this.elementLoader().getElements().length
        },
        
		getTarget: function() {
			
			//return false;
			// loads the first slide  - note called right up at the top of the execution stack, just after the initial LOAD() call
			//dc 
			
            var t, e = this._screen._container;
			
			
			//console.log("this screen container in getTarget() is ");
			//console.log(this._screen._container);
			
			
            if (this._xml.attr("target") && "" != this._xml.attr("target")) {
				
				//console.log("current target content is "); 
				
				//console.log(this.elementLoader().getElementById(this._xml.attr("target")));
				
                 if (t = this.elementLoader().getElementById(this._xml.attr("target"))) return t._container;
             
			    if (get(this._xml.attr("target"))) return get(this._xml.attr("target"))
            
			}
            return this._target ? this._target : e
        },
        
	
		
		create: function() { // the main function that renders the slides
            if (this._container = this.createDiv(), this._initialAnimComplete = !1, this.elementHasNoAnim() && (this._initialAnimComplete = !0), "screen" == this._type) {
               
			    var t = {
                          screenXML: this._xml,
                          screenElement: this
                        },
                    
					e = new Screen(t);
					
					
			    console.log("this element screen is:");
				console.log(e);
			   
						
                this._elementScreen = e, e.load(this._container, !0)
				
            }
			
			//console.log("in create() , this.type is: " );
			//console.log(this._type);
			
			
            if ("text" === this._type && new Text({
                    element: this,
                    container: this._container,
                    xml: this._xml
                }), "box" === this._type  && new Box({
                    element: this,
                    container: this._container,
                    xml: this._xml
                }), "button" === this._type && new Button({    //dc
                    element: this,
                    container: this._container,
                    xml: this._xml
                }), "custom" === this._type) switch (this._xml.attr("type").toLowerCase()) {
                
				
				case "certificate":
				   
					xml: this._xml;
					console.log(this._xml);
					//this.createLabel(this._xml);
					//this.createDiv();
					
					
					/*
					new Button({    //dc
									element: this,
									container: this._container,
									xml: this._xml
                                 }); 
								 */
								 
				  
				    /*
				     new Section({    //dc
									element: this,
									container: this._container,
									xml: this._xml
                                 }); 
						*/		 
								 
					  new Label({    //dc
									element: this,
									container: this._container,
									xml: this._xml
                                 }); 			 
				 
				  
				  
				break;
				
				case "mcq":
				    
                    this._custom = new MCQ({
                        element: this
                    });
                    break;
                
				case "quiz":
				  
				  //return false;
				  
                    this._custom = new Quiz({  // this instantiates the whole quiz //dc
                        element: this
                    });
					
					//this._custom.tester();
				
					//this._custom.clearQuestionContainer();
		            //this._custom.clearTimeoutContainer(); 
					//this._custom.loadQuestion(12); //dc
					
					
                    break;
					
					
					/*
					if(this._xml.attr("type") == 'name_participant') {
					   xml: this._xml;
					   console.log(xml);
					   console.log("PARTICIPANT NAME!! TRYING TO FORM !!");
					}
					
					break;
					*/
					
					
                default:
                    alert("unrecognised custom screen element " + this._xml.attr("type"))
            }
        },
        elementHasNoAnim: function() {
            return null == this._anim || "none" == this._anim || "hidden" == this._anim || "hide" == this._anim || "disabled" == this._anim || "disable" == this._anim || 0 == this._eventq
        },
        registerAnimComplete: function() {
            this._initialAnimComplete = !0, this._animOnComplete ? this._screen.doClickEventById(this._animOnComplete, this) : !1, this.elementLoader().checkBatchAnimsComplete()
        },
        createDiv: function() {
           
		    var t = create({
                type: "div",
                id: this._id
            });
			
            if (xmlAttrStr(this._xml, "position")) {
                var e = this._xml.attr("position");
                $(t).css("position", e)
            } else $(t).css("position", "absolute");
            if ($(t).css("visibility", "hidden"), xmlAttrStr(this._xml, "margin") && $(t).css("margin", this._xml.attr("margin")), xmlAttrStr(this._xml, "margin-top") && $(t).css("margin-top", getUnit(this._xml.attr("margin-top"))), xmlAttrStr(this._xml, "margin-right") && $(t).css("margin-right", getUnit(this._xml.attr("margin-right"))), xmlAttrStr(this._xml, "margin-bottom") && $(t).css("margin-bottom", getUnit(this._xml.attr("margin-bottom"))), xmlAttrStr(this._xml, "margin-left") && $(t).css("margin-left", getUnit(this._xml.attr("margin-left"))), xmlAttrStr(this._xml, "min-width") && $(t).css("min-width", getUnit(this._xml.attr("min-width"))), xmlAttrStr(this._xml, "max-width") && $(t).css("max-width", getUnit(this._xml.attr("max-width"))), xmlAttrStr(this._xml, "min-height") && $(t).css("min-height", getUnit(this._xml.attr("min-height"))), xmlAttrStr(this._xml, "max-height") && $(t).css("max-height", getUnit(this._xml.attr("max-height"))), xmlAttrStr(this._xml, "z-index") && $(t).css("z-index", this._xml.attr("z-index")), xmlAttrStr(this._xml, "float") && $(t).css("float", this._xml.attr("float")), xmlAttrStr(this._xml, "clear") && $(t).css("clear", this._xml.attr("clear")), xmlAttrStr(this._xml, "display") && $(t).css("display", this._xml.attr("display")), xmlAttrStr(this._xml, "overflow") && $(t).css("overflow", this._xml.attr("overflow")), "image" != this._type && (xmlAttrStr(this._xml, "className") && $(t).addClass(this._xml.attr("className")), xmlAttrStr(this._xml, "class") && $(t).addClass(this._xml.attr("class"))), this._scroll) {
                var i = this._xml.attr("scroll").toLowerCase(),
                    n = -1 != i.indexOf("x"),
                    s = -1 != i.indexOf("y"); - 1 != i.indexOf("touch") ? supportsTouch && (n ? $(t).css("overflow-x", "auto") : s ? $(t).css("overflow-y", "auto") : ($(t).css("overflow-y", "auto"), $(t).css("overflow-x", "auto"))) : n ? ($(t).css("overflow-x", "auto"), $(t).css("overflow-y", "hidden")) : s ? ($(t).css("overflow-y", "auto"), $(t).css("overflow-x", "hidden")) : ($(t).css("overflow-y", "auto"), $(t).css("overflow-x", "auto")), $(t).css("-webkit-overflow-scrolling", "touch")
            }
            var r = -1 != this._x.toString().indexOf("+=") || -1 != this._x.toString().indexOf("-=") || "match" === this._x.toString().toLowerCase() || "center" === this._x.toString().toLowerCase() || -1 != this._y.toString().indexOf("+=") || -1 != this._y.toString().indexOf("-=") || "match" === this._y.toString().toLowerCase() || "center" === this._y.toString().toLowerCase();
            r && (this._relativePos = !0), r || (this._x = parseInt(this._x), this._y = parseInt(this._y), $(t).css("left", this._x), $(t).css("top", this._y));
            var a = this.elementLoader().getPreviousElement(this._id);
            if (this._width) {
                var o = this._width;
                "match" === o.toString().toLowerCase() && a && (o = a._width), this._width = o, $(t).css("width", this._width)
            }
            if (this._height) {
                var l = this._height;
                "match" === l.toString().toLowerCase() && a && (l = a._height), this._height = l, $(t).css("height", this._height)
            }
            return $(this.getTarget()).append(t), $(t).css("opacity", 0), t
        },
        positionRelatively: function() {
            var t = this.elementLoader().getPreviousElement(this._id);
            if (t) {
                if ("match" === this._x.toString().toLowerCase() && (this._x = parseInt(t._x)), -1 != this._x.toString().indexOf("+=") || -1 != this._x.toString().indexOf("-=")) {
                    var e = parseInt(this._x.substring(2));
                    this._x = parseInt(t._x) + parseInt($(t._container).innerWidth()) + e
                }
                if ("center" === this._x.toString().toLowerCase() && (this._x = t._x + $(t._container).innerWidth() / 2 - $(this._container).innerWidth() / 2), "match" === this._y.toString().toLowerCase() && (this._y = parseInt(t._y)), -1 != this._y.toString().indexOf("+=") || -1 != this._y.toString().indexOf("-=")) {
                    var i = parseInt(this._y.substring(2));
                    this._y = parseInt(t._y) + parseInt($(t._container).innerHeight()) + i
                }
                "center" === this._y.toString().toLowerCase() && (this._y = t._y + $(t._container).innerHeight() / 2 - $(this._container).innerHeight() / 2)
            }
            this._x = parseInt(this._x), this._y = parseInt(this._y), $(this._container).css("left", this._x), $(this._container).css("top", this._y)
        },
        animate: function() {
			
			
			
            for (var t, e = this._anim.split("|"), i = 0; i < e.length; i++) {
                    
					 //e[i] = "left";
			   
			    t = e[i];
				
				
				
                var n; - 1 != t.indexOf("top_") && (n = t.split("_")[1], -1 != n.toString().indexOf("+=") && (n = this._y + Number(n.toString().replace("+=", ""))), -1 != n.toString().indexOf("-=") && (n = this._y - Number(n.toString().replace("-=", ""))), -1 != n.toString().indexOf("orig") && (n = this._y), t = "moveTop");
                var s; - 1 != t.indexOf("left_") && (s = t.split("_")[1], -1 != s.toString().indexOf("+=") && (s = this._x + Number(s.replace("+=", ""))), -1 != s.toString().indexOf("-=") && (s = this._x - Number(s.replace("-=", ""))), -1 != s.toString().indexOf("orig") && (s = this._x), t = "moveLeft");
                var r;
                if (-1 != t.indexOf("scale_")) r = parseFloat(t.split("_")[1]), r > 1 && (r /= 100), t = -1 != t.indexOf("lscale_") ? "lscale" : "scale";
                else {
                    var a = 1;
                    "undefined" != typeof TweenMax && this._container._gsTransform && (a = this._container._gsTransform.scaleX), r = a
                }
                var o = 50,
                    
				
					l = (this._animease, this);
                
				
				//console.log("e  is now : ");  //dc shows a type of animation, like 'right' or 'left' 
				//console.log(e);
				
				
				
				switch (t) {
                    case "remove":
                        var h = {
                            opacity: 0
                        };
                        $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: function() {
                                $(l._container).remove()
                            }
                        });
                        break;
                    case "enable":
                    case "enabled":
                        "button" == this._type ? this.enableBtn() : this.enable();
                        break;
                    case "disable":
                    case "disabled":
					     
						 //console.log("button disabld in essemble_core_dc.js"); //dc
						 
                        if ("button" == this._type) {
							
							//console.log("we are animating the button to opacity .3...");
							
                            var h = {
                                opacity: 0 
                            };
							
							/* the below is animating the confirm button into a .3 opacity state on the part 2 multiple choice question */  
							
                            //$(this._container).delay(1e3 * this._animdelay).animate(h, { 
                             //   duration: 1e3 * this._animtime,
                             //   easing: JQEase(this._animease),
                              //  complete: this.disableBtn.bind(this)
                            //})
                        } else this.disable();
                        break;
                    case "hidden":
                    case "hide":
                        var h = {
                            opacity: 0
                        };
                        $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: function() {
                                l.registerAnimComplete.bind(l), $(l._container).css("visiblity", "hidden")
                            }
                        });
                        break;
                    case "alpha":
                    case "show":
                        
						
						
						var h = {
                            opacity: 1,
                            queue: !1
                        };
                        $(this._container).css("visibility", "visible"), $(this._container).css("opacity", 0), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "top":
                        var h = {
                                top: this._y,
                                opacity: 1
                            },
                            c = -1 * parseInt($(this._screen._container).css("height")) - o;
                        $(this._container).css("top", c), $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "bottom":
                        var h = {
                                top: this._y,
                                opacity: 1
                            },
                            c = parseInt($(this._screen._container).css("height")) + o;
                        $(this._container).css("top", c), $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "left":
                        var h = {
                                left: this._x,
                                opacity: 1
                            },
                            c = -1 * parseInt($(this._screen._container).css("width")) - o;
                        $(this._container).css("left", c), $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "right":
					 
					  // this case moves the very first slide into position //dc
					  // and probably the other slides as well
                        
						//console.log("this container is");
					    //console.log(this._screen._container);// this contains the very first slide, and all that follow ? //dc
						
						var h = {
                                left: this._x, // animates the slide to the left //dc
                                opacity: 1
                            },
							
							
							
                            c = parseInt($(this._screen._container).css("width")) + o ;
                        $(this._container).css("left", c), $(this._container).css("visibility", "visible"), $(this._container).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "moveTop":
                    case "movetop":
                        var h = {
                            top: n,
                            opacity: 1
                        };
                        $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "moveLeft":
                    case "moveleft":
                        var h = {
                            left: s,
                            opacity: 1
                        };
                        $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "drop":
                        var h = {
                            scale: 1,
                            opacity: 1
                        };
                        $(this._container).css("scale", 3), $(this._container).css("opacity", 0), $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "scale":
                        var h = {
                            scale: r,
                            opacity: 1
                        };
                        $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    case "lscale":
                        break;
                    case "scaleup":
                        var h = {
                            scale: 1,
                            opacity: 1
                        };
                        $(this._container).css("scale", 0), $(this._container).css("opacity", 0), $(this._container).css("visibility", "visible"), $(this._container).stop(!0, !1).delay(1e3 * this._animdelay).animate(h, {
                            duration: 1e3 * this._animtime,
                            easing: JQEase(this._animease),
                            complete: this.registerAnimComplete.bind(this)
                        });
                        break;
                    default:
                        $(this._container).show(), $(this._container).css("visibility", "visible"), $(this._container).css("opacity", 1), this.registerAnimComplete()
                }
            }
        },
        registerEvents: function() {
            if ("disabled" != this._anim && "disable" != this._anim) {
                this._arRollovers = [], this._arRollouts = [], this._arClicks = [];
                var t, e = !1,
                    i = !1,
                    n = !1,
                    s = this._event.split(",");
                $(this._container).off();
                for (var r = 0; r < s.length; r++)
                    if (t = this._screen.getEventById(s[r])) {
                        if (t._arRollovers.length > 0) {
                            e = !0;
                            for (var a = 0; a < t._arRollovers.length; a++) this._arRollovers.push(t._arRollovers[a])
                        }
                        if (t._arRollouts.length > 0) {
                            i = !0;
                            for (var a = 0; a < t._arRollouts.length; a++) this._arRollouts.push(t._arRollouts[a]), this._screen.fireEvents(t._arRollouts[a], this)
                        }
                        if (t._arClicks.length > 0) {
                            n = !0;
                            for (var a = 0; a < t._arClicks.length; a++) this._arClicks.push(t._arClicks[a])
                        }
                    } else alert("event not found:" + s[r]);
                e && (this._screen.fireEvents(this._arRollouts, this), $(this._container).on("mouseenter", this.mouseOverHandler.bind(this)), $(this._container).css("cursor", "pointer")), i && $(this._container).on("mouseleave", this.mouseOutHandler.bind(this)), n && ($(this._container).on("click", this.clickHandler.bind(this)), $(this._container).css("cursor", "pointer"), $(this._container).children().css("cursor", "pointer"))
            }
        },
        mouseOverHandler: function() {
            this._screen.fireEvents(this._arRollovers, this)
        },
        mouseOutHandler: function() {
            this._screen.fireEvents(this._arRollouts, this)
        },
        clickHandler: function(t) {
            t.stopPropagation(), t.preventDefault(), this._screen.fireEvents(this._arClicks, this)
        },
        disable: function() {
            $(this._container).off(), $(this._container).css("cursor", "default"), $(this._container).children().css("cursor", "default")
        },
        enable: function() {
            this._event && (("disabled" == this._anim || "disable" == this._anim) && (this._anim = "alpha"), this.registerEvents())
        },
        disableBtn: function() {
            this.disable(), $(this._container).find("input").css("cursor", "default"), $(this._container).css("visibility", "visible"), $(this._container).css("zoom", "1"), $(this._container).css("display", "block"), $(this._container).css("opacity", .2)
        },
        enableBtn: function() {
            this.enable(), $(this._container).find("input").css("cursor", "pointer"), $(this._container).css("visibility", "visible"), $(this._container).css("opacity", 1), $(this._container).css("display", "block")
        },
        rollover: function() {
            for (var t = 0; t < this._arRollovers.length; t++) this._screen.fireEvents(this._arRollovers[t], this)
        },
        rollout: function() {
            for (var t = 0; t < this._arRollouts.length; t++) this._screen.fireEvents(this._arRollouts[t], this)
        },
        hide: function() {
            $(this._container).css("opacity", 0), $(this._container).hide()
        },
        show: function() {
            $(this._container).css("opacity", 1), $(this._container).show()
        },
        reset: function() {
            this.elementHasNoAnim() || this.hide(), ("hidden" === this._anim || "hide" === this._anim) && this.hide(), this._event && (this._arRollouts.length > 0 && this.rollout(), this.enable()), this._container.x = this._x, this._container.y = this._y
        }
    }, 
	
	
	
	ElementLoader.prototype = {
        
		
		load: function() {
			
			//console.log("LOAD CALLED"); // the initial load of the application //dc 
		
			
            this._batchFinished = !1, this.attachPreloader();
			
            for (var t = 0; t < this._arElements.length; t++) {
			     
				   //console.log("elements length is ");
				   //console.log(this._arElements.length);
			
				  this._arElements[t]._container && (this._arElements[t]._container = null),
				  
				  this._arElements[t].create(); // this is where all elements are created
			}
			
            for (var t = 0; t < this._arElements.length; t++)
                if (this._arElements[t]._elementLoader = this, "image" == this._arElements[t]._type) {
                    
					var e = this._arElements[t]._xml.text(),
					
					i = new PxLoaderImage(e);
                    
					i.element = this._arElements[t], this._loader.add(i);
					
					//console.log("text is");
					//console.log(e);
                }
				
            this._loader.start(); // initiates the slideshow
        },
        delayRelativePos: function(t) {
            setTimeout(function() {
                t.positionRelatively(), t.animate()
            }, 100)
        },
        delayAnimate: function(t) {
            setTimeout(function() {
                t.animate() //dc // the very first animation
            }, 100)
        },
        checkAllAnimsComplete: function() {
            for (var t = !0, e = 0; e < this._arElements.length; e++)
                if (!this._arElements[e]._initialAnimComplete) {
                    t = !1;
                    break
                }
            return t
        },
        checkBatchAnimsComplete: function() {
            var t = this.checkAllAnimsComplete();
            if (t && !this._batchFinished) {
                this._batchFinished = !0;
                for (var e = 0; e < this._arElements.length; e++)
                    if (this._arElements[e]._event && this._arElements[e].registerEvents(), "custom" == this._arElements[e]._type) try {
                        this._arElements[e]._custom.init()
                    } catch (i) {}
                    this._onAnimsComplete && (this._onCompleteParams ? this._onCompleteScope[this._onAnimsComplete](this._onCompleteParams) : this._onCompleteScope[this._onAnimsComplete]())
            }
        },
        showPreloader: function() {
            $(this._preloader).show()
        },
        attachPreloader: function() {
            if (!get("preloader")) {
                var t = ($(this._screen._container).innerWidth() - 34) / 2,
                    e = ($(this._screen._container).innerHeight() - 34) / 2;
                this._preloader = create({
                    type: "div",
                    id: "preloader"
                }), $(this._preloader).css("position", "absolute"), $(this._preloader).css("z-index", 999), $(this._preloader).css("left", t), $(this._preloader).css("top", e), $(this._preloader).addClass("preloader"), $(this._preloader).hide(), this._screen._container.appendChild(this._preloader), this.showPreloader()
            }
        },
        removePreloader: function() {
            pl = this._preloader, this._screen._container.removeChild(pl), this._preloader = null
        },
        hidePreloader: function() {
            this._preloader && $(this._preloader).animate({
                opacity: 0
            }, {
                duration: 1e3,
                complete: this.removePreloader.bind(this)
            })
        },
        getElements: function() {
            return this._arElements
        },
        addElement: function(t) {
            t._elementLoader = this, this.getElements().push(t)
        },
        getElementById: function(t) {
            for (var e = null, i = 0; i < this._arElements.length; i++)
                if (this._arElements[i]._id == t) {
                    e = this._arElements[i];
                    break
                }
            return e
        },
        getElementByType: function(t) {
            for (var e = null, i = 0; i < this._arElements[i].length; i++)
                if (this._arElements[i]._type == t) {
                    e = this._arElements[i];
                    break
                }
            return e
        },
        getPreviousElement: function(t) {
            for (var e = null, i = 0; i < this._arElements.length; i++)
                if (this._arElements[i]._id == t && i > 0) {
                    e = this._arElements[i - 1];
                    break
                }
            return e
        },
        getNextElement: function(t) {
            for (var e = null, i = 0; i < this._arElements.length; i++)
                if (this._arElements[i]._id == t && i < this._arElements.length - 1) {
                    e = this._arElements[i + 1];
                    break
                }
            return e
        },
        stop: function() {
            this._loader.addCompletionListener(function() {
                return !1
            }), this._loader.addProgressListener(function() {
                return !1
            }), this._onLoadComplete = null, this._onAnimsComplete = null
        }
    };