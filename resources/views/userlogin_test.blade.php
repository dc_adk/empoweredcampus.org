<!DOCTYPE html>
<head>
<title>ED User Login</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Fonts  -->
<link href='https://fonts.googleapis.com/css?family=Patrick+Hand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="landing-page-resources/js/davis.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="landing-page-resources/css/styles.css">

</head>
<body class="scroll-up">

	<!-- Fixed navbar -->
    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
          <h3 style="margin:0;">EmpoweredED</h3>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      		<ul class="nav navbar-nav">
	           
          	</ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!-- Begin page content -->

    <!--Jumbotron-->
    <div class="jumbotron absolute">
        <img src="landing-page-resources/img/davis-bg.jpg" alt="The Davis Project" />
    	   <div class="v-align-text">
                <div class="container center-block">
                  
                  
                       {{ Form::open(array('url' => 'userlogin')) }}
                      
                            {{ Form::label('email', 'Email Address') }}
                            {{ Form::text('email') }}
                            
                            {{ Form::label('password', 'Password') }}
                            {{ Form::text('password') }}
                            
                            {{ Form::submit('Login') }}
    
        
                      {{ Form::close() }}
                  
                  
                </div>
            </div>
	</div> 

	

<!-- Footer -->
    <footer class="footer">
      <div class="container">
        <h4 class="text-center"><strong>EmpoweredEd</strong></h4>    

		<ul class="list-inline text-center">
		  <li> <a href="#"><i class="fa fa-facebook"></i></a></li>
		  <li><a href="#"><a href="#"><i class="fa fa-twitter"></i></a></li>
		  <li><a href="#"><a href="#"><i class="fa fa-linkedin"></i></a></a></li>
		</ul>

		<p class="text-center">Brought to you by <a href="https://mentalhealthscreening.org/">Screening for Mental Health Inc.</a></p>
	    	 
	    </p>
      </div>
    </footer>

</body>
</html>
