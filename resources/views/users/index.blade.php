
<!DOCTYPE html>
<head>
<title>Sign up for EmpowerED</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Fonts  -->
<link href='https://fonts.googleapis.com/css?family=Patrick+Hand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="../../landing-page-resources/js/davis.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../landing-page-resources/css/styles.css">

</head>
<body class="ed-bg-list-users relative">

	<div class="container">
		
        <div class="row">
			
                  
                         <h3 class="text-center">EmpowerED User List </h3> 
                         <h5 class="text-center"><a href="{{ url('/signup') }}">Register New Users Here</a></h5> 
                            
                              <table class="user_list">
                                   <th class="name_sort">
                                      <a href="{{ url('/user') }}{{ isset($users->sort) ? '/first_name/'. $users->sort : '/first_name/asc' }}">User First Name</a>
                                   </th>
                                   
                                   <th class="name_sort">
                                      <a href="{{ url('/user') }}{{ isset($users->sort) ? '/last_name/'. $users->sort : '/last_name/asc' }}">User Last Name</a>
                                   </th>
                                  
                                   <th>
                                      User Email
                                   </th>   
                                   <th> 
                                      Slides Viewed
                                   </th>
                                   <th class="excel_export"> 
                                      <a href="{{ url('/user') }}{{ isset($users->export_link) ? $users->export_link : '/export-to-excel' }}">Export to Excel</a>
                                   </th>
                                   
                                     @foreach ($users as $user)
                                     
                                         <tr>
                                            <td>{{$user->first_name}}</td>
                                            <td>{{$user->last_name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->current_slide }}</td>
                                            <td><a href="{{ url('/user')}}/{{$user->id}}/edit">Edit</a> | <a class="user_delete"  href="{{ url('/user')}}/{{$user->id}}/delete">Delete</a></td>
                                         </tr>
                                        
                                     @endforeach
                                   
                                   
                              </table>         
             
		    </div>
	   </div>
 
</body>
<script>
    
       $(function() {
           
           $('.user_delete').click(function(event) {
               
					var result = confirm("You are about to delete this user. Would you like to proceed?");
                    if (result) {
                        return true;
                    }
                    else {
                        
                        return false;
                    }
             });
			 
			 
			$('.name_sort a').click(function() {
				
				var sorting_href = $(this).attr('href');
				console.log(sorting_href);
				
				$('.excel_export a').attr('href',sorting_href+'/export-to-excel');
			});
			 
           
       });


</script>


</html>