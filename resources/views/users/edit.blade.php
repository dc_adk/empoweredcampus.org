
<!DOCTYPE html>
<head>
<title>Sign up for EmpowerED</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Fonts  -->
<link href='https://fonts.googleapis.com/css?family=Patrick+Hand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="{{ url('/user')}}/../../landing-page-resources/js/davis.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="{{ url('/user')}}../../landing-page-resources/css/styles.css">

</head>
<body class="ed-signup-bg relative">
<div class="v-align-text">
	<div class="container">
		<div class="row">
			 <div class="col-md-offset-4 col-md-4 padB50">
                    <div class="ed-signup-form">
                         <h3 class="text-center" style="margin:0;">Edit User</h3>
                            
                              <section class="edit_customer">
                            
                                    <form action="{{ url('/user/update')}}/{{$user->id}}" method="POST">
                                        @include('users.partials.form')
                                        {{ csrf_field() }}
                                         <input type="submit" value="Update User"/>
                                    </form>
                                
                                     <div class="user_msg">
                                           
                                                {{$user->update_success}} 
                                       
                                     </div>  
                                
                                
                                    <form id="pw_reset" action="{{ url('/user/reset_password')}}/{{$user->id}}" method="POST">
                                           <div class="form_control">
                                              <label>Password Reset</label>
                                              <input type="text" name="new_user_password" placeholder="enter new password" value=""/>
                                              <div class="form_error"></div>
                                           </div> 
                                           <div class="form_control">
                                              <label>Confirm new password</label>
                                              <input type="text" name="confirm_user_password" placeholder="confirm new password"  value=""/>
                                              <div class="form_error"></div>
                                           </div> 
                                           {{ csrf_field() }}
                                           <input type="submit" value="Reset Password"/>
                                    </form>
                                    
                               	  <div class="user_msg">
                                            @if(Session::has('reset_success'))  
                                                {{Session::get('reset_success')}} 
                                            @endif
                                  </div>    
                                   
                              </section>         
                         
                    </div>
			</div>
		</div>
	</div>
</div>
</body>
<script>
   $(function() {
	   
	       $('.edit_customer [name="confirm_user_password"]').blur(function() {
			     
			      var pw1 = $('.edit_customer [name="new_user_password"]').val();
				  var pw2 = $(this).val();
				  
				  if(pw1 != pw2) {
					  
					  $(this).next('.form_error').text('Passwords must match.');
				  }  
		   });
		   
		  $('#pw_reset').on( "submit", function() {
			
			  if($('.edit_customer [name="new_user_password"]').val() != $('.edit_customer [name="confirm_user_password"]').val() ) {
				
				  return false; 
			  }
		  });
		   
   });

</script>
</html>