<!DOCTYPE html><head>
<title>EmpowerED</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon"  href="{{ asset('favicon.ico') }}" type="image/x-icon"/>

<!-- Fonts  -->

<link href='https://fonts.googleapis.com/css?family=Patrick+Hand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="landing-page-resources/js/davis.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="landing-page-resources/css/styles.css">

@include('analytics')

</head>
<body class="scroll-up">

    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
          <h3 style="margin:0;">EmpoweredED</h3>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      		<ul class="nav navbar-nav">
                <li><a href="{{ url('/userlogin') }}">Log In</a></li>
          	</ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!-- Begin page content -->

    <!--Jumbotron-->
    <div class="jumbotron absolute">
        <img src="landing-page-resources/img/davis-bg.jpg" alt="The Davis Project" />
    	   <div class="v-align-text">
                <div class="container center-block">
                  <h1>Welcome to EmpowerED</h1>
                      <p>A one hour interactive training designed to get you rethinking how your campus talks about weight and body image. Providing advice, best practices, and stories from real college students living with eating disorders, EmpowerED promotes wellness and recovery-focused strategies that can be utilized in the counseling center and throughout your school's campus.</p>
                      <p><a class="btn btn-primary btn-lg" href="{{ url('/userlogin') }}" role="button">Get Started</a></p>
                </div>
            </div>
	</div> 

	<div class="container">
		<div class="row">
			 <div class="col-md-offset-1 col-md-10 padB50">
                    <h2 class="text-center"><strong>You Can Play a Critical Role in the Recovery Process</strong></h2>
				    <h4 class="text-center">College and university counseling centers aren't meant to take the place of in-patient treatment programs, but staff can still play a significant role in student recovery.</h4>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			 <div class="col-md-offset-1 col-md-5 relative">
			 	<div class="">
                    <div class="container center-block text-center map-block reflect">
                      <img src="landing-page-resources/img/reflect-ico.png" />
                    </div>
                </div>
			 </div>
  			 <div class="col-md-5">
  			 	<h3 class="step-title"><strong>Take a Closer Look</strong></h3>
  			 	<p class="step-desc">Self-reflection activities will help you think about your own thoughts and feelings related to eating, exercise, and weight.</p>
  			 </div>
		</div>
	</div>
	
	<div class="container mobile-hide">
	    <div class="row">
			 <div class="col-md-offset-1 col-md-5 relative" style="height:200px">
			     <div class="absolute davis-mapping-left-one" style=""></div>
            </div>
            <div class="col-md-5 relative overflow-hide" style="height:195px">
                <div class="absolute davis-mapping-right-one" style=""></div>
            </div>
        </div>
	</div>

	<div class="container">
		<div class="row">
			 <div class="col-md-offset-1  col-md-5 col-md-push-5 relative">
			 	<div class="">
                    <div class="container center-block text-center map-block learn">
                      <img src="landing-page-resources/img/learn-ico.png" />
                    </div>
                </div>
			 </div>
			 <div class="col-md-5 col-md-pull-5">
  			 	<h3 class="step-title"><strong>Learn &amp; Test</strong></h3>
  			 	<p class="step-desc">Everything from eating disorder basics and definitions, to common myths and the important facts and figures you need to know.</p>
  			 </div>
		</div>
	</div>
	
	<div class="container mobile-hide">
	    <div class="row">
			 <div class="col-md-offset-1 col-md-5 relative overflow-hide" style="height:195px">
			     <div class="absolute davis-mapping-left-two" style=""></div>
            </div>
            <div class="col-md-5 relative" style="height:200px">
                <div class="absolute davis-mapping-right-two" style=""></div>
            </div>
        </div>
	</div>

	<div class="container">
		<div class="row">
			 <div class="col-md-offset-1 col-md-5 relative">
			 	<div class="">
                    <div class="container center-block text-center map-block campus">
                      <img src="landing-page-resources/img/campus-ico.png" />
                    </div>
                </div>
			 </div>
  			 <div class="col-md-5">
  			 	<h3 class="step-title"><strong>Implement Changes on Your Campus</strong></h3>
  			 	<p class="step-desc">Put what you've learned to the test and actively make changes on your campus. Read the student stories and learn how you can make a difference for students living with eating disorders on your campus.</p>
  			 </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			 <div class="col-md-offset-1 col-md-10 bordTop">
				 <div class="container maxWid100">
				 	<h3 class="text-center">Get started on learning how to promote wellness on your campus</h3>
				 	<p class="text-center" style="padding-top:20px;"><a class="btn btn-primary btn-lg" href="{{ url('/userlogin') }}" role="button">Get Started on EmpowerED</a></p>
				 </div>
			</div>
		</div>
	</div>


<!-- Footer -->
    <footer class="footer">
      <div class="container">
        <h4 class="text-center"><strong>EmpoweredEd</strong></h4>    

		<ul class="list-inline text-center">
		  <li> <a href="https://www.facebook.com/HelpYourselfHelpOthers"><i class="fa fa-facebook"></i></a></li>
		  <li><a href="https://twitter.com/HYSHO"><i class="fa fa-twitter"></i></a></li>
		  <li><a href="https://www.linkedin.com/company/982084?trk=tyah&trkInfo=tarId%3A1409243114984%2Ctas%3Ascreening%20for%20mental%20healt%2Cidx%3A1-1-1"><i class="fa fa-linkedin"></i></a></a></li>
		</ul>

		<p class="text-center">Brought to you by <a href="https://mentalhealthscreening.org/">Screening for Mental Health Inc.</a></p>
	    	 
	    </p>
      </div>
    </footer>

</body>
</html>