<!DOCTYPE html>
<head>
<title>Sign up for EmpowerED</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Fonts  -->
<link href='https://fonts.googleapis.com/css?family=Patrick+Hand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="landing-page-resources/js/davis.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="landing-page-resources/css/styles.css">

@include('analytics')


</head>
<body class="ed-signup-bg relative">
<div class="v-align-text">
	<div class="container">
		<div class="row">
			 <div class="col-md-offset-4 col-md-4 padB50">
                    <div class="ed-signup-form">
                         <h3 class="text-center" style="margin:0;">EmpowerED</h3>
                       
                         <h5 class="text-center">Sign up successful for {{ $theEmail }}</h5>
                         
                        <br/>
                        <h5 class="text-center"><a href="{{ url('/userlogin') }}">Log In Here</a></h5>
                    </div>
			</div>
		</div>
	</div>
</div>
</body>
</html>