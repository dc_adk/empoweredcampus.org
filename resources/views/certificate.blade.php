<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Multiple Choice Quiz Engine | Responsive</title>
	<!-- Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Hind:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Patrick+Hand+SC' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/lib/css/styles.css') }}" >
     
   @include('analytics')
    
    
 </head>


<body><input type="hidden" id="csrf" value="{{ csrf_token() }}" />

   
    <div id="container"  class="relative">
    
           <div id="content-wrapper" class="diploma-wrapper">    
			<img src="info_slides/images/part2/certificate.jpg" class="full-width img-responsive"/>
								
			 <div class="name_of_participant">
                       {!! $name !!}
                </div>
                
          </div>
    
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
    
    <script src="{{ asset('/lib/js/jquery.js') }}"></script>
    <script src="{{ asset('/lib/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('/lib/js/jquery.transit.min.js') }}"></script>
    <script src="{{ asset('/lib/js/essemble_core_dc.js') }}"></script>
    <script src="{{ asset('/lib/js/mcq.js') }}"></script>
    <script src="{{ asset('/lib/js/quiz.js') }}"></script>
    
    <script>
    
      function getParameterByName(name) {
										name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
										var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
										results = regex.exec(location.search);
										return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                                      }
	
	
	var desired_slide = getParameterByName('q');  
	var session_info = {};
	var csrf_token ='';
	
	//console.log("desired slide is");
	//console.log(desired_slide);
	
	csrf_token = $("#csrf").val();
	
	
  @if(isset($user_info))
	
		var user_info = {!! json_encode($user_info) !!};
		var current_slide; 
		
		(desired_slide =='')?current_slide = user_info.current_slide:user_info.current_slide =  desired_slide;
		
		//console.log("current slide is:" );
		//console.log(user_info.current_slide);
		
		 session_info =   {                      //dc
							  "user_id":user_info.user_id,
							  "user_name":user_info.user_name,
							  "session_id":"",
							  "current_slide":user_info.current_slide,
							  "token":csrf_token
						   };
		
		
		//console.log("user info going into project is: ");
		//console.log(user_info);
	
	@endif
	
	console.log(session_info);
	
	var quiz;

    function init(){
        
        //create the screen object which loads the xml and creates all screen elements
		
        quiz = new Screen({id:"myQuiz", xmlPath:"{{ asset('lib/xml/quiz_adk_all.xml') }}",session:session_info});
        
        //choose a target div
        var targetDiv = get("container");
        
        //load it
        quiz.load(targetDiv,false);
    }
    
    //kick off
    $(document).ready(function() {
        
		//init();
		
		$("#container").append('<div class="project_banner_wrapper"><section class="project_banner" id="project_title"> <span style="font-size:14px;">EmpowerED Part I</span></section></div>');
		$('.project_banner_wrapper').append('<div class="progress"><div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow=""><span class="sr-only">60% Complete</span></div></div>');
		//$("#container").append('<progress  max="100"></progress>');
		$('progress').hide();
		
	
		
    });
    
    </script>


<footer>


 
  </footer>
  
</body>
</html>
