<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Multiple Choice Quiz Engine | Responsive</title>
	<!-- Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Hind:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Patrick+Hand+SC' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
    
     <!-- Bootstrap core CSS -->
    <link href="lib/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ asset('/lib/css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('/info_slides/styles/normalizer.css') }}" rel="stylesheet">  
    <link href="{{ asset('/info_slides/styles/styles.css') }}" rel="stylesheet">
    
 
    <link href="{{ asset('/lib/css/aw.css') }}" rel="stylesheet">
  
    <link href="{{ asset('/lib/css/kd.css') }}" rel="stylesheet">
    <link href="{{ asset('/info_slides/styles/landers-master.css') }}" rel="stylesheet">
    
    @include('analytics')
 </head>


<body><input type="hidden" id="csrf" value="{{ csrf_token() }}" />

   
    <div id="container"  class="relative">
    
    
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
    
    <script src="{{ asset('/lib/js/jquery.js') }}"></script>
    <script src="{{ asset('/lib/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('/lib/js/jquery.transit.min.js') }}"></script>
    <script src="{{ asset('/lib/js/essemble_core_dc.js') }}"></script>
    <script src="{{ asset('/lib/js/mcq.js') }}"></script>
    <script src="{{ asset('/lib/js/quiz.js') }}"></script>
    
    <script>
    
      function getParameterByName(name) {
										name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
										var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
										results = regex.exec(location.search);
										return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                                      }
	
	
	var desired_slide = getParameterByName('q');  
	var session_info = {};
	var csrf_token ='';
	
	//console.log("desired slide is");
	//console.log(desired_slide);
	
	csrf_token = $("#csrf").val();

	
  @if(isset($user_info))
	
		var user_info = {!! json_encode($user_info) !!};
		var current_slide; 
		
		(desired_slide =='')?current_slide = user_info.current_slide:user_info.current_slide =  desired_slide;
		
		//console.log("current slide is:" );
		//console.log(user_info.current_slide);
		
		 session_info =   {                      //dc
							  "user_id":user_info.user_id,
							  "user_name":user_info.user_name,
							  "session_id":"",
							  "current_slide":user_info.current_slide,
							  "token":csrf_token
						   };
		
		
		//console.log("user info going into project is: ");
		//console.log(user_info);
	
	@endif
	
	console.log(session_info);
	
	var quiz;

    function init(){
        
        //create the screen object which loads the xml and creates all screen elements
		
        quiz = new Screen({id:"myQuiz", xmlPath:"{{ asset('lib/xml/quiz_adk_all.xml') }}",session:session_info});
        
        //choose a target div
        var targetDiv = get("container");
        
        //load it
        quiz.load(targetDiv,false);
    }
    
    //kick off
    $(document).ready(function() {
        
		init();
		
		$("#container").append('<div class="project_banner_wrapper"><section class="project_banner" id="project_title"> <span style="font-size:14px;">EmpowerED Part I</span></section></div>');
		$('.project_banner_wrapper').append('<div class="progress"><div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow=""><span class="sr-only">60% Complete</span></div></div>');
		//$("#container").append('<progress  max="100"></progress>');
		$('progress').hide();
		
		
		$("#participant_name").val("HERE!!!");
		
    });
    
    </script>


<footer>

  <!--
   
    <a data-toggle="modal" data-target="#emily" class="modal-trigger">Emily</a>
	<a data-toggle="modal" data-target="#kyle" class="modal-trigger">Kyle</a>	
	<a data-toggle="modal" data-target="#rachel" class="modal-trigger">Rachel</a>	
	<a data-toggle="modal" data-target="#sarah" class="modal-trigger">Sarah</a>


  -->

  <div class="modal fade" id="emily" tabindex="-1" role="dialog" aria-labelledby="emilysStory">
		<div class="modal-dialog large" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body left">
					<h2 class="modal-title blue" id="story-name">Emily</h2>
					<div class="inner">
						<p class="question">You mentioned in your personal narrative that being in a college environment was triggering due to the body-shaming, diet-dominated rhetoric of peers. What do you think colleges can do to send a different message to students about their physical appearance?</p>
						<p>I think that frequently, colleges focus on only one end of the student health spectrum. In many dining halls and campus eateries, posters and signs advertise low-calorie meal options, offer nutritional information about food items, and re- mind students to follow a “healthy diet.” Rarely is there discussion or promotion of body diversity, widely varying individual dietary needs, or “intuitive eating” in these spaces. I think we are at a point where nationally, we recognize that the “one size  fits all” health plan is a fallacy. For individuals struggling with or recovering from eating disorders, a “healthy diet” looks different than it might for an individual without a history of eating disorders. I think that the best message a college could promote would be “balance.”</p>
						<p class="question">How much control do you think a college has in influencing the dialogue or attitudes students have about body diversity, self-worth, and knowledge about important issues like eating disorders?</p>
						<p>Colleges are hugely influential. In my opinion and experience, one of the best ways colleges can use this influence is by creating an environment in which students feel comfortable reaching out for help. Frequently, conversations about eating disorders are reserved for counselors and doctors, which can make some students who may be struggling feel like options for them are simply not available. Similarly, if individuals without experience dealing with or knowledge of eating disorders are not exposed to the issue or are not well informed, it makes it very difficult for them to support peers and deconstruct harmful stereotypes and rhetoric about eating disorders.</p>
						<p class="question">
							What do you think are the most important things for a counselor to say or not to say when someone with an eating disorder reaches out to them for help?</p>
						<p>For some people, reaching out for help with an eating disorder is very difficult, and if that person does not feel comfortable, validated, or affirmed by the counselor in whom he or she has confided, it can be a huge deterrent to recovery. It can also be very helpful for counselors to share with these individuals the other resources available to him or her. Is there a dietician with eating disorder experience on or near campus? Are there doctors or nurses in the health center with eating disorder experience?
						</br></br>One of the least constructive things for anyone (parents, peers, counselors) to say is “You don’t look like you have/ had an eating disorder,” or “You look healthy.”  ere are a variety of different kinds of eating disorders, each of which looks different for each person. Not only can these appearance-based comments contribute to an individual’s body image concerns, but they also allow the individual to focus on the superficial manifestations of their eating disorders rather than the root causes.</p>
						<p class="question">You spoke a lot about the support network provided by your family and how it played a large role in you seeking out help. From your own experiences, do you think that colleges can provide this much needed support for students as well?</p>
						<p>I think they can and should. As I mentioned above, I think one of the best things colleges can do is create a “safe space” for positive conversations about eating disorders to occur. In terms of providing support networks for individuals, I think it is very important that colleges have professionals on campus with eating disorder experience. Counselors, doctors, dietitians, and deans and student life staff with experience and education are better equipped to recognize “red flags,” constructively support students in reaching out for help, and facilitate students’ recovery, among other things.</p>
						<p class="question">What advice would you give to those who are transitioning from treatment to a college campus to help them continue to stay on track with the freedom that college life provides?</p>
						<p>My advice would be to establish a college support network prior to transitioning. Making contact with your support persons before you transition to life on a college campus can remove some of the “transition time” pressure. I also recommend prioritizing self-care. As I often remind myself, it is not selfish to care for yourself; in fact, physical, emotional, and spiritual self-care are critical to everything else you do. Self-care can take many forms – reading a book for pleasure, painting your nails, removing yourself from a conversation about dieting.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="kyle" tabindex="-1" role="dialog" aria-labelledby="kylesStory">
		<div class="modal-dialog large" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body right">
					<h2 class="modal-title blue" id="story-name">Kyle</h2>
					<div class="inner">
						<p class="question">What do you think colleges can do to send a different message to students about their physical appearance?</p>
						<p> I think overall promoting body acceptance and differences in all people. I think there is a large amount of pressure in college for students, both male and female, to be attractive and sexy. I think addressing differences in people and having students engage in conversions about the overall social pressures of college can hopefully help decrease the peer pressure, bullying, etc.</p>
						<p class="question">How much control do you think a college has in influencing the dialogue or attitudes students have about body diversity, self-worth, and knowledge about important issues like eating disorders?</p>
						<p>I think that colleges themselves have probably little influence, but can help promote the conversation by encouraging groups or clubs that address these issues. I think that this could happen through student support/counseling centers. I also think this goes back to the pressures in college around partying and sex culture. I think that colleges can work to address this.  There is also possibilities to promote body confidence and self-compassion by leading groups or having speakers come to talk about these issues.</p>
						<p class="question">What do you think are the most important things for a counselor to say or not to say when someone with
an eating disorder reaches out to them for help?</p>
						<p> The most important thing is to listen and take it seriously, no matter how close or far the behaviors are from the clinical diagnosis of an eating disorder is.  There are a plethora of people living in college who have sub-clinical eating disorders and if they come to a counselor with a problem, then they think it’s a problem.  This should not be taken lightly. I think that the counselor should also seek the appropriate people to work with the student if they do not think they can or have the knowledge to do so.</p>
						<p class="question">From your own experiences, do you think that colleges can provide the necessary support?</p>
						<p>Yes to some extent. But if the student needs higher levels of care, then that must be honored. I also think that the counselor needs to be somewhat intuitive about the gap between what the student is saying versus what they are actually doing. Meaning, if the client says they are eating their meals but continues to look withdrawn and malnourished, then they are most likely not telling the truth.</p>
						<p class="question">What advice would you give to those who are transitioning from treatment to a college campus to help them continue to stay on track with the freedom that college life provides?</p>
						<p>It’s all about choices. You have the make the choiceddds that lead to recovery. You can’t compare yourself to what other people are doing, eating, etc. You are different than others, and that’s a good thing. Be honest with yourself and reach out if needed. Have point people on campus to go to for a variety of reasons.</br></br>Separately, I wanted to add that males are often overlooked when it comes to EDs. I think that males with EDs often experience different behaviors (steroid/supplement abuse, over exercising). I think that on larger campuses with a strong focus on sports/athletics, men with ED will just look like “good athletes”. Coaches and administration involved in athletics should be trained and given the knowledge of EDs and what it may look like in their athletes.  There is always the pressure and competitiveness to win, but athletes who are struggling are at higher risk of injury or even death. More work needs to be done in this area.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="rachel" tabindex="-1" role="dialog" aria-labelledby="rachelsStory">
		<div class="modal-dialog large" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body right">
					<h2 class="modal-title" id="story-name">Rachel</h2>
					<div class="inner">
						<p class="question">You mentioned in your article in Self that you were a pre-med student. Do you think that rigorous programs like pre-med and pre-law see a higher rate of students struggling with mental health disorders?</p>
						<p>110% yes--these issues tend to strike people who have a specific layer of being perfectionists or creating a greater good and who punish themselves when they don’t reach their unattainable goals.  There’s this constant need to be in control of everything because you feel so out of control.</p>
						<p class="question">You mention several times that your eating disorder had a huge impact on your social life. Did any of your friends ever try to voice concern? If they did, do you think you would have listened?</p>
						<p>During college 50% of my weight decline occurred. I lost enough that it was very apparent. Frankly I think even my most well intentioned friends had no idea what to say.  That’s what I hear a lot. Someone knows someone with an eating disorder in their life but it can be difficult to approach the subject knowing that on a certain level the person is going to be really offended.</p>
						<p class="question">We see that a lot of students are okay with taking an anonymous screening for eating disorders but they have some of the lowest rates for saying they plan to seek help. How can counselors help work to change this?</p>
						<p> The truth is, many individuals with eating disorders don’t want to change it.  They are completely caught in their disease and their mental illness has completely taken over. When you have an eating disorder it can feel like you are outside of or separate from yourself.  The wise mind is no longer functioning, the eating disorder is functioning.  The eating disorder wants to live and when you’re hearing the “no” response you are hearing from someone entrenched by their eating disorder.  They don’t want to get fat.  They’re thinking, “I’m fine the way that I am.” It took several months of being in an eating disorder treatment center, taking medication, and restoring enough weight (brain was functioning) to realize that I wanted to get well.</p>
						<p class="question">What do you think are the most important things for a counselor to say or not to say when someone with an eating disorder reaches out to them for help?</p>
						<p><b>Focus on What’s Going on on the Inside:</b> If they have someone with an eating disorder and have just started working together it’s important to keep talk about the individual’s body to themselves. Keep observations about their bodies out of the conversation. Talk about the inside -- what’s going on.</br></br><b>Set Parameters for Discussion:</b> Talking with someone who is beginning to weight-restore- -it’s important to know on a personal level from them if it’s okay to say you look great or you look healthy. You don’t want to inadvertently trigger their eating disorder--making them think they are fat.</br></br><b>Keeping Weight Talk Out of It: </b>Also it’s important for the counselor to have a conversation with the individual who has an eating disorder letting them know that part of their job is keeping them alive. At some point, someone needs to be knowing your weight. A counselor could say, “Am I allowed to speak with your nutritionist-- knowing that I won’t speak to you about it directly?”.  They shouldn’t be weighing themselves. Not engaging with client about the number. Weight conversations should be strictly between them and their nutritionist.</br></br><b>Maintain Honesty:</b> Creating a space where honesty is the number one core value of the relationship is critical.  The minute the student is being dishonest, you have lost them. A person with an eating disorder is still going to lie (probably) but is this going to be kept to a minimum. Don’t want this to be a space where they just tell you what you want to hear.</br></br><b>Not punishing or shaming when things go wrong:</b> Everyone slips up but the hope is that it will start going happening less frequently as time goes on.</p>
						<p class="question">You spoke about you and your Dad’s shared struggle with disordered eating. Do you think it’s important that counseling centers reach out to parents/families of students with information about what to look out for their child’s mental health?</p>
						<p>I totally agree that that would be helpful. Creating some sort of brochure or informational packet as it relates to eating disorders. Me and my dad would look at nutritional labels together which would inform what we bought. We would go for runs together. We had constant conversations about food--what’s okay and what’s not okay.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="sarah" tabindex="-1" role="dialog" aria-labelledby="sarahsStory">
		<div class="modal-dialog large" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body left">
					<h2 class="modal-title" id="story-name">Sarah</h2>
					<div class="inner">
						<p class="question">What do you think colleges can do to send a different message to students about their physical appearance?</p>
						<p>I think a concerted effort for campaigns acknowledging and promoting body acceptance, body confidence, and size diversity is essential for college campuses. Encouraging students to engage in media literacy, provide awareness about how to create a body positive environment, and promoting the importance of sensitive and informed language are all helpful in creating a community that is safe for all bodies.</br></br>When I was in college there was an open meeting held to  find ways to help the gym feel safe for all students. It was a wonderful opportunity for me to engage in some advocacy for myself and also opened my eyes to the ways that the gym can be difficult for students for many different reasons- for trans folks, people in vary body shapes and sizes, people with varying levels of ability, etc.</p>
						<p class="question">How much control do you think a college has in influencing the dialogue or attitudes students have about body diversity, self-worth, and knowledge about important issues like eating disorders?</p>
						<p>For students who may already be experiencing an eating disorder, disordered eating, or negative body image, the college can only assist them by guiding them toward professional mental health support. However, general discourse about body confidence and body diversity can be encouraged by the student health services center and the student counseling center. A concerted effort to examine these issues and promote awareness is a benefit to the whole campus and could help those struggling seek support services.</p>
						<p class="question">What do you think are the most important things for a counselor to say or not to say when someone with an eating disorder reaches out to them for help?</p>
						<p><b>Take the person seriously:</b> EDs and related issues aren’t for attention, a misguided cry for help, or a phase.</br></br><b>Be discerning:</b> When I reported to my college nutritionist that I had eaten 5 Hershey kisses, she immediately assumed I was  fine and did not have an ED. Little did she know that those 5 Hershey kisses were haunting my every waking moment and that I had obsessively exercised to “work them off.”</br></br><b>Listen carefully:</b> Provide a comprehensive assessment for ED behaviors, self harm, trauma, and suicidality.</br></br><b>Know your scope:</b> If you are not qualified to assist the individual due to their ED behaviors, refer them to someone who can.</br></br><b>Always be gently curious</b> about whether or not the client is telling you the whole story- I held back from the whole truth when talking to my college counselor. I was deeply ashamed so told her a half-version of what was going on.</p>
						<p class="question">From your own experiences, do you think that colleges can provide the necessary support?</p>
						<p>To some extent. If I had been truthful to my college counselor about the depth of my ED thoughts and behaviors, I doubt she would have felt comfortable working with me. I was restricting, bingeing and over-exercising which she did help me with, however if I had been purging more often or restricting more severely, I doubt a once a week session (I only got 8) would have sufficed.</p>
						<p class="question">What advice would you give to those who are transitioning from treatment to a college campus to help them continue to stay on track with the freedom that college life provides?</p>
						<span>1. Always remember that every moment you have the opportunity to make a recovery choice. Every moment is new. So you misjudge your plate and eat a little more than normal and start to freak out? You have a recovery choice to make. You can distract, accept, and move on, or you can choose the ED. Saw your roommates scale and got on it, and didn’t like the number? You have a recovery choice to make. You can distract, accept, and move on, or you can choose the ED. College is rife with opportunities for the ED to take control. It’s up to YOU to be prepared and make the best recovery choice you can, and then have compassion for yourself no matter what you choose.</span>
						<span>2. Have supports readily available. Have a list of distraction techniques, coping skills, and people you feel comfortable talking with on hand at all times.  That way, you are setting yourself up for success as much as possible.</span>
						<span>3. Get involved in a recovery group on campus. It will help with isolation and accountability.</span>
						<span>4. Check in with your team on a regular basis, even if you are feeling okay.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
 
  </footer>
  
</body>
</html>
