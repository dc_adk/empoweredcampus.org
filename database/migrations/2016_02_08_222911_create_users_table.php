<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		
		 // NOTE - AFTER THIS SHELL IS CREATED, WE NEED TO ADD THE ACTUAL CODE TO CREATE THE TABLE //dc
		 
		 // AND MAKE SURE USERS TABLE IS CREATED FIRST, BEFORE THE 'USER ROLES' SET OF TABLES COMES IN //dc
		 
		 Schema::create('users', function($table){
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->boolean('is_admin');
            $table->timestamps();
        });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
