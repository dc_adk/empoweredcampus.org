<?php

/*
|--------------------------------------------------------------------------
| Routes File 
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

/* There was a problem working directly with native laravel session states  (see SessionGuard.php file ) 
 I had to use my own, raw php session objects and glue them into the system as best as I could. This will be evident

I'm certain this can be done in a much more graceful way. But the prescribed methods Auth->user, Auth::check(); did not work after 
a page was refreshed. Ie, laravel session information would not persist from page to page.  //dc
*/
 
use App\Model\User;


Route::get('/', function () {
   
	return view('home');
	
});


Route::get('/home', function () {
   
	return view('home');
	
});

// Routes for live site 


/*
// http://stackoverflow.com/questions/28557408/how-to-check-a-token-csrf-on-controller

In Laravel 5 middleware replaces filters. 
This is also true for CSRF. The middleware is enabled by default and is handled in App\Http\Middleware\VerifyCsrfToken.

*/

Route::group(['middleware' => ['web']], function () {
 

		 Route::get('slides', function() {  
	 
		 session_start();
	
		//print_r($_SESSION['user_session_info']); exit;
		
		if(isset($_SESSION['user_session_info']['id'])  && $_SESSION['user_session_info']['id'] !='') {
			
			
			   $user_id = $_SESSION['user_session_info']['id'];
			   $user_name = $_SESSION['user_session_info']['name'];
			   $current_slide = $_SESSION['user_session_info']['current_slide'];
			
			   return view('slides',['user_info' =>['user_id' =>  $user_id, 'user_name'=> $user_name, 'current_slide' => $current_slide]]);
		}
		else {
			 return Redirect::to('userlogin');
		}
		
		// NOTE !  the above session variable now holds all the users info that was created in the 
		// vendor/laravel/framework/src/Illuminate\Auth\SessionGuard.php table // 
		// all user data can now be stored persistently from page to page as needed 
	
      });


      Route::get('signup', function () { // only allow access to signup for admin users
		  
		        session_start();
	
		       //print_r($_SESSION['user_session_info']); exit;
		
				if(isset($_SESSION['user_session_info']['id'])  && $_SESSION['user_session_info']['id'] !='' && $_SESSION['user_session_info']['is_admin'] ==1) {
				
					   $user_id = $_SESSION['user_session_info']['id'];
					   $user_name = $_SESSION['user_session_info']['name']; 
					   $current_slide = $_SESSION['user_session_info']['current_slide'];
					
					   return view('signup');
					
				 }
				 
				 else {
					 return Redirect::to('home');
				}
	
       });
 
        Route::get('certificate', function () {
           
		   //echo public_path();
		   //exit;
		   session_start();
		   
		   if(isset($_SESSION['user_session_info'])) {
			  
			 // if($_SESSION['user_session_info']['id'] !='') {
			   
			   $user_id = $_SESSION['user_session_info']['id'];
			   $user_name = $_SESSION['user_session_info']['name'];
		  
		       $user = User::find($user_id);
		   
		       $pdf = App::make('dompdf.wrapper');
        
		    //return $pdf->loadHTML('/var/www/html/empowered/resources/views/certificate.blade.php')->stream('download.pdf');
		 
		       $name = $user->name;
			   $email = $user->email;
		       $cert_name_tag = str_replace(' ', '-', strtolower($name));
		       $date = date('m-d-Y');
		 
		          $cert_title =  '/var/www/vhosts/empoweredcampus.org/httpdocs/pdfs/empowered_certificate'. "_". $cert_name_tag. "_" . $date .".pdf";
		 
				   $pdf_data = $pdf->loadHTML(view('certificate', ['name'=>$user->name]))->output();
				   
				    Mail::send('emails.awarded', ['user' => $user], function ($m) use ($pdf_data,$user) {
                                                 // NOTE - pad_show needs to be used as an argument to use it below //dc
												 
		            // NOTE - this email has to be carefully set up in the env file with the right credentials
                      
					 $m->from('no-reply@empowerededcampus.org', 'Empowered ED');   
					  
                    // $m->to('dan@cyclecommunications.net', $user->name)->subject('Empowered ED Training');
					
					$m->to($user->email, $user->name)->subject('Empowered ED Training');
				    $m->attachData($pdf_data, "Certificate.pdf"); // this is where the pdf will go
				
                });
				
				 echo "mail sent";
				
				 //return view('home'); 
				
		   }
		   else {
			   
			 // echo "user not logged in ";  
			      return view('home'); 
		   }
		 // }
	
       });
   
   
   
   Route::get('userlogin', function () {
	 
	 //session_destroy();
	 //Session::flush();
	
	 return view('userlogin');
   });
   
  


   
   Route::post('/signup','RegisterController@doRegister');
   Route::post('/slides/post-ajax','SlidesController@post_ajax');
   Route::post('/slides/post-ajax-certificate','SlidesController@post_ajax_certificate');
  
   
  
   
   Route::group(array("before"=>"csrf"), function(){  
	
        Route::post('/userlogin', array("as" => "update-role-permissions", "uses" => 'LoginController@doLogin'));
    });
	

    // Route::post('/userlogin', array("as" => "update-role-permissions", "uses" => 'LoginController@doLogin'));

	         Route::get('user','UserController@index'); 
			 Route::get('user/first_name/asc','UserController@index'); 
			 Route::get('user/first_name/desc','UserController@index'); 
			 Route::get('user/last_name/asc','UserController@index'); 
			 Route::get('user/last_name/desc','UserController@index');  
			 
			 // excel export routes 
			 
			 Route::get('user/export-to-excel','UserController@export_to_excel');
			 Route::get('user/first_name/asc/export-to-excel','UserController@export_to_excel');
			 Route::get('user/first_name/desc/export-to-excel','UserController@export_to_excel');
			 
			 Route::get('user/last_name/asc/export-to-excel','UserController@export_to_excel');
			 Route::get('user/last_name/desc/export-to-excel','UserController@export_to_excel');
			 
			 
			 Route::get('user/{id}/edit','UserController@edit'); 
			 
			 Route::post('user/reset_password/{id}','UserController@reset_password');
			 Route::post('user/update/{id}','UserController@update');
			 Route::get('user/update/{id}','UserController@update'); //allow for page refreshes
			 Route::get('user/{id}/delete','UserController@delete'); 
	
});
