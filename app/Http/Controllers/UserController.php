<?php 
namespace App\Http\Controllers;
use App\Model\User;
use App\Model\Role;
use Illuminate\Support\Facades\Request;
use Auth;
use View;
use Input;
use Mail;
use Excel;


use Illuminate\Support\Facades\Hash;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	|
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
		 
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	 public function index()
    {
     
					 $users =  $this->get_users();//User::all();
					 return View::make('users.index')->with('users', $users);
	 
		  
      }
	
	
	
	public function get_users() {
		
		 session_start();
         if(isset($_SESSION['user_session_info']['is_admin']) && $_SESSION['user_session_info']['is_admin'] == 1) {
			 
			  if(Request::segment(2) == '')  {
			  
			      $users =  User::all();
			      return $users;
			  }
			 
			  if(Request::segment(2) == 'first_name') {
				 
						if(Request::segment(3) == 'desc') {
							 $users = User::all()->sortByDesc("first_name"); 
							 $users->sort = 'asc';
							 $users->export_link = '/first_name/desc/export-to-excel';
						}
						
						 if(Request::segment(3) == 'asc') {
							$users = User::all()->sortBy("first_name");
							$users->sort = 'desc';
							$users->export_link = '/first_name/asc/export-to-excel';
						}
					 
					   // return View::make('users.index')->with('users', $users);
				   
				   }
				   
				
			 if(Request::segment(2) == 'last_name') {
				 
						if(Request::segment(3) == 'desc') {
							 $users = User::all()->sortByDesc("last_name"); 
							 $users->sort = 'asc';
							 $users->export_link = '/last_name/desc/export-to-excel';
						}
						
						 if(Request::segment(3) == 'asc') {
							$users = User::all()->sortBy("last_name");
							$users->sort = 'desc';
							$users->export_link = '/last_name/asc/export-to-excel';
						}
					 
					    //return View::make('users.index')->with('users', $users);
				   
				   }  
		        }
				
		 else { // in not admin credentials, redirect to home
			    return redirect('/');     
		   }		    
		
		 return $users;
		
	}
	
	 public function export_to_excel() {
		
		
		 if(Request::segment(2) == 'export-to-excel')  {
			  
			      $users =  User::all();
			     
		  }
		else {
		      $users = $this->get_users();
		}
		
		 Excel::create('UsersExcelSheet', function($excel) use($users) {
                       
								    $excel->sheet('Users', function($sheet) use($users) {
									$sheet->fromArray($users);

                                  });

                       })->export('xls');
	 }
	
	
	 public function edit() {
		
	  session_start();	 
	  if(isset($_SESSION['user_session_info']['is_admin']) && $_SESSION['user_session_info']['is_admin'] == 1) { 
	      $user_id = Request::segment(2);
		  $user = User::find($user_id);
		  return View::make('users.edit')->with('user', $user);
	  }
	  else {
		  return redirect('/'); 
	   }
 
	 }
	 

	  public function update() {
		
		 $user_id = Request::segment(3);
		 $user = User::find($user_id);
		 session_start();	 
		 
		 if(isset($_SESSION['user_session_info']['is_admin']) && $_SESSION['user_session_info']['is_admin'] == 1) { 
			
		 if(Input::get('first_name') != '') { // in the case of a post, we'll have input values to update customer record, otherwise page is just a GET refresh 
				
				 $first_name = Input::get('first_name');
				 $last_name = Input::get('last_name');
				 $email = Input::get('user_email');
				 $current_slide = Input::get('user_current_slide');
				 $user->first_name = $first_name;
				 $user->last_name = $last_name;
				 $user->email = $email;
				 $user->current_slide = $current_slide;
				
				 if($user->save()) {
					 	 
			        $user->update_success = "User updated!";
 
				 }
				 else {
					  $user->update_success = "There was an upser update error. Please try again.";
				 } 
			 
		  }
	     
		  return redirect('user/'.$user_id.'/edit')->with('user', $user);
		 }
		 else {
		  return redirect('/'); 	 
		 }
		 
	 }
	
	 
	 
	 
	 public function reset_password() {
		
		session_start();
		
		if(isset($_SESSION['user_session_info']['is_admin']) && $_SESSION['user_session_info']['is_admin'] == 1) {  
		  $user_id = Request::segment(3);
		  $user = User::find($user_id);
		  $user->password = Hash::make(Input::get('new_user_password'));
		  $user->save(); 
		  
		   return redirect('user/'.$user_id.'/edit')->with('reset_success','Password reset successful!'); 
		}
		else {
			return redirect('/'); 
		}
	 }
	 
	 
	 public function delete() {
	   
	   session_start();
	   
		if(isset($_SESSION['user_session_info']['is_admin']) && $_SESSION['user_session_info']['is_admin'] == 1) {
			$user_id = Request::segment(2);	
			$user = User::find($user_id);    
			$user->delete();
			return redirect('user');
		}
		else {
			return redirect('/'); 
		}
		 
	 }
	 
	
}
