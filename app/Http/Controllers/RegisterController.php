<?php namespace App\Http\Controllers;
use App\Model\User;
use App\Model\Role;

use View;
use Input;
use Mail;

use Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	|
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	 public function doRegister(Request $request)
    {
        

		$user = new User;
		$user->first_name = Input::get('first_name');
		$user->last_name = Input::get('last_name');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
		
		
	    /*
		 $this->validate($request, [
                                    'email' => 'required|email|unique:users' // works, just reloads page 

       							  ]);
		
	   */
	  
	  $validator = Validator::make(
									
									array(
									
										 'email' =>  Input::get('email')
										),
									array(
									   
										'email' => 'required|email|unique:users'
									)
								);
	
	
	  if ($validator->fails())
			{
				
				 return Redirect::to('signup')->with('register_error','This email has already registered.');
				
			}
	
        $user->save();
        $theEmail = Input::get('email');
		
		$username = $user->first_name . " " . $user->last_name;
		$pwd = Input::get('password');
		
		
		Mail::send('emails.registration_confirmation', ['username' => $username,'pwd'=> $pwd], function($message) {
            $message->to(Input::get('email'), Input::get('username'))
                ->subject('Empowered Registration Confirmation');
        });
		
        return View::make('signup_success')->with('theEmail', $theEmail); 
    }
}
