<?php namespace App\Http\Controllers;
use Auth;
use Redirect;
use App;
use App\Model\User;
use App\Model\Role;

//use Entrust; // created as a custom alias in config/app.php

use View;
use Input; 
use Session;
use Mail;
use Illuminate\Support\Facades\Hash;
use Barryvdh\DomPDF\ServiceProvider;


class SlidesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Slides Controller
	|--------------------------------------------------------------------------
	|
	|
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	 public function index()
    {
		
		
		//session_start();
		//$_SESSION['user_session_info'];
		
		// NOTE !  the above session variable now holds all the users info that was created in the 
		// vendor/laravel/framework/src/Illuminate\Auth\SessionGuard.php table // 
		// all user data can now be stored persistently from page to page as needed //dc
					
		//return Redirect::to('slides',array('user_id' =>  '987', 'username'=> 'John Doe', 'quiz_step' => '7'));
	 }
	 
	 
	 public function post_ajax()
    {
        //echo "DEBUG FROM AJAX"; // works 
	 
		 $user = User::find( Input::get('user_id') );
		 
		 if( $user ) {
			 
			      if(Input::get('current_slide') < 98) {
					     $user->current_slide = Input::get('current_slide');
				  }
					
				 else {	
					     $user->current_slide = 0;
				    }
					
					
                    $user->save();
         }   
		
		
		return array('user_id'=>Input::get('user_id'),'current_slide'=>Input::get('current_slide'));

    }
	
	
	public function post_ajax_certificate() {
		
		     $user = User::find( Input::get('user_id') );
			 $user_id = $user->id;
		     $name = $user->first_name . " " . $user->last_name;
		 
		     $cert_name_tag = str_replace(' ', '-', strtolower($name));
		     $date = date('m-d-Y');
		 
		       $pdf = App::make('dompdf.wrapper');
			
			    if( $user ) { // prepare to send valid user an award email //dc
				 
							//return $pdf->loadHTML('/var/www/html/empowered/resources/views/certificate.blade.php')->stream('download.pdf');
						 
							 //$name = $user->name;
							 $cert_name_tag = str_replace(' ', '-', strtolower($name));
							 $date = date('m-d-Y');
							 
						     $cert_title = '/var/www/vhosts/empoweredcampus.org/httpdocs/pdfs/empowered_certificate' . "_". $cert_name_tag. "_" . $date .".pdf";
						     // create an award pdf with the new live asset path
						 
								           $pdf_data = $pdf->loadHTML(view('certificate', ['name'=>$name]))->output() ;
								   
											Mail::send('emails.awarded', ['user' => $user], function ($m) use ($pdf_data,$user) {
										    // send certificate to user										 
											$m->from('empoweredcampus.org', 'Empowered ED'); // NOTE - this email has to be carefully set up in the env file with the right credentials
											$m->to($user->email, $name)->subject('Empowered ED Training');
											$m->attachData($pdf_data, "Certificate.pdf"); // attach award pdf before sending 
								
								      });
								
								     //echo "certificate mailed";
			      } // end if user exists   
	  
		
	}
			
    
}
