<?php namespace App\Http\Controllers;
use Auth;
use Redirect;
use App\Model\User;
use App\Model\Role;

//use Entrust; // created as a custom alias in config/app.php
use Illuminate\Http\Request;
use Illuminate\Auth\SessionGuard;

use View;
use Input; 
use Session;
//use App\Http\Middleware\VerifyCsrfToken;



use Illuminate\Support\Facades\Hash;

class LoginController extends Controller {



	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	|
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
		$this->_token ='';
		
		//$this->middleware('guest'); // this was kicking out users that would try to log in , but if there was a preexisting session, 
		// middleware guest would kick them out before the doLogin even kicked in.
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	 public function doLogin(Request $request)
    {
		
		if(Auth::check()){
			
			  Auth::logout(); // an additional measure to make sure users are logged out , before they are logged in 
		}
		
		if(isset($_SESSION)) {
			$request->session()->flush();
			session_destroy();	
		}
		
		$credentials = Input::only('email','password');
		   
		   
		   if(Auth::attempt($credentials)) {
		
			    $user = Auth::user();
			
			    Auth::loginUsingId($user->id); // referring to Illuminate/Auth/SessionGuard.php

				$user_id = $_SESSION['user_session_info']['id'];
				$user_name = $_SESSION['user_session_info']['name'];
				$current_slide = $_SESSION['user_session_info']['current_slide'];
				
				if($user->is_admin == 1){
					
					 $_SESSION['user_session_info']['is_admin'] = 1;
					 return Redirect::to('signup');
				}
				
				else {
					
		
					  return Redirect::intended('slides')->with( 'user_id', $user_id )
															     ->with( 'user_name', $user_name )
															     ->with( 'current_slide', $current_slide);
					
					 // NOTE ! with redirects, you need to use the 'with' method to pass arguments
				}
		     }
	else {
			
			 return View::make('userlogin',['data'=>['login_error'=>'Login credentials appear to be incorrect. Please try again.']]);
				   
			}
		
	  
    }
}
