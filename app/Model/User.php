<?php namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


use Input;  //dc 

//use  Illuminate\Support\Facades\Input;
//use Zizaco\Entrust\Traits\EntrustUserTrait; //dc

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Bican\Roles\Models\Role;


//class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
//class User extends Eloquent {


class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract   //dc
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission; //dc

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'first_name','last_name','email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	

	public function roles() {
     
	     return $this->belongsToMany('Role','assigned_roles');
    }
	
	
  public function get_role(){
		//Role::attachRole;
	}
	
public function create_roles(){	 // works well ! //dc 
    
		$adminRole = Role::create([
										'name' => 'admin_user',
										'slug' => 'admin.user',
										'description' => '', // optional
										'level' => 1, // optional, set to 1 by default
	                             ]);
								
		
		
		$siteuser = Role::create([
										'name' => 'client_user',
										'slug' => 'client.user',
										'level' => 2
									]);
									
	
     }
	 
	 
	 
	 
	

}
